@extends('backEND.layouts.index')

@section('page-content')

<div class="pcoded-content">
   <div class="pcoded-inner-content">
      <div class="main-body">
         <div class="page-wrapper">
            {{-- BREADCRUMD --}}
            <div class="page-header">
               <div class="page-header-title">
                  <h4>Edit User</h4>
               </div>
               <div class="page-header-breadcrumb">
                  <ul class="breadcrumb-title">
                     <li class="breadcrumb-item">
                        <a href="index-2.html">
                        <i class="icofont icofont-home"></i>
                        </a>
                     </li>
                     <li class="breadcrumb-item"><a href="#!">User</a></li>
                     <li class="breadcrumb-item"><a href="#!">Edit User</a></li>
                  </ul>
               </div>
            </div>
            {{-- END BREADCRUMD --}}
            <div class="page-body">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="card">
                        <div class="card-header">
                           <h5>Edit User</h5>
                           <br/>
                           <p>dssfddfssd</p>
                        </div>
                        
                        <div class="card-block">
                        <form method="post" action="{{ URL::to('users',$data['users']->id)}}" enctype="multipart/form-data" id="myReset">
                            {{ csrf_field() }}
                            {{ method_field('put') }}
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nama Lengkap</label>
                                <div class="col-sm-10">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input 
                                       type="text" 
                                       class="form-control" 
                                       name="name" 
                                       placeholder="Nama Lengkap" 
                                       value="{{ isset($data['users']) ? $data['users']->name : '' }}"
                                    >
                                    @if (session()->has('name'))
                                       <span class="messages">*  {{ session('name') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input 
                                       type="email" 
                                       name="email"
                                       class="form-control" 
                                       id="email" 
                                       placeholder="contoh : admin@example.com" 
                                       value="{{ isset($data['users']) ? $data['users']->email : '' }}"
                                    >
                                    @if (session()->has('email'))
                                       <span class="messages">*  {{ session('email') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Tempat Tanggal Lahir</label>
                                <div class="col-sm-5">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input 
                                       type="text" 
                                       class="form-control" 
                                       name="tempat_lahir" 
                                       placeholder="Tempat Lahir" 
                                       value="{{ isset($data['users']) ? $data['users']->tempat_lahir : '' }}"
                                    >
                                </div>
                                <div class="col-sm-5">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input 
                                       id="dropper-default" 
                                       class="form-control" 
                                       type="text" 
                                       name="tanggal_lahir" 
                                       placeholder="Tanggal Lahir" 
                                       value="{{ isset($data['users']) ? date('d-m-Y', strtotime($data['users']->tanggal_lahir)) : '' }}"
                                    >
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">No Handphone</label>
                                <div class="col-sm-10">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input 
                                        type="number" 
                                        class="form-control" 
                                        name="phone" 
                                        placeholder="No Handphone" 
                                        pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==13) return false;" 
                                        value="{{ isset($data['users']) ? $data['users']->phone : '' }}"
                                    >
                                    <span class="messages"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Upload Photo</label>
                                <div class="col-sm-4">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    @if($data['users']->photo == "" || $data['users']->photo == null)
                                        <input 
                                            type="file" 
                                            name="photo" 
                                            class="form-control"
                                        >
                                    @else
                                        <div class="pull-right">
                                            <a href="{{ URL('UpdatePhotoEmpty/' . $data['users']->id) }}" title="Delete Photo">
                                                <img src="{{ URL::asset('public/asset_admin/image/close.png') }}" style="position: absolute;width: 35px;margin-top: 1px;margin-left: -115px;box-shadow: 0 0 25px -2px #b7b7b7;">
                                            </a>
                                        </div>
                                        <img src="{{ asset('public/'.$data['users']->photo_path) }}" alt=""  style="border: 1px solid #fff;width: 300px;">
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Password</label>
                                <div class="col-sm-10">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input 
                                       type="password" 
                                       class="form-control" 
                                       id="password" 
                                       name="password" 
                                       placeholder="Password" 
                                       value="{{ isset($data['users']) ? $data['users']->password : '' }}"
                                    >
                                    <span>
                                        <button class="btn btn-default reveal" type="button" onclick="myFunction()" style="margin-top: -40px;height: 40px;float: right;"><i class="ion-eye"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Repeat Password</label>
                                <div class="col-sm-10">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input 
                                       type="password" 
                                       class="form-control" 
                                       id="repeat-password" 
                                       name="password_confirmation" 
                                       placeholder="Ulangi password diatas" 
                                       value="{{ isset($data['users']) ? $data['users']->password : '' }}"
                                    >
                                    <span>
                                        <button class="btn btn-default reveal" type="button" onclick="myPassword()" style="margin-top: -40px;height: 40px;float: right;"><i class="ion-eye"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Role User</label>
                                <div class="col-sm-10">
                                    @foreach($getRole as $index => $dataRole)
                                    <div class="form-check form-check-horizontal radio-danger">
                                        <label class="form-check-label">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input 
                                                class="form-check-input" 
                                                type="radio" 
                                                name="role" 
                                                value="{{$dataRole->id}}"
                                                @if ($dataRole->id == $data['users']->role)
                                                    checked
                                                @endif
                                            > {{$dataRole->roles_user}}
                                        </label>
                                    </div>
                                    @endforeach

                                    @if (session()->has('role'))
                                       <span class="messages"> | * {{ session('role') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2"></label>
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-danger m-b-0">Reset</button>
                                    <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                </div>
                            </div>
                        </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
        </div>
         <!-- UNTUK SETTING COLOR -->
         <div id="styleSelector"></div>
      </div>
   </div>
</div>

@endsection

@section('js')
<script type="text/javascript" src="{{ asset('public/asset_admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/asset_admin/assets/pages/advance-elements/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/asset_admin/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/asset_admin/bower_components/datedropper/datedropper.min.js') }}"></script>
<script>
   $('#dropper-default').dateDropper({
      theme: 'leaf'
   });
</script> 
<script>function myFunction() { var x = document.getElementById("password"); if (x.type === "password") { x.type = "text"; } else { x.type = "password"; } }</script>
<script>function myPassword() { var x = document.getElementById("repeat-password"); if (x.type === "password") { x.type = "text"; } else { x.type = "password"; } }</script>
@endsection