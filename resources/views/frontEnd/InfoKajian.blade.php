<div class="col-md-3" style="margin-top: -268px;">
    <div class="text-center" style="padding: 5px;margin: 0 auto;width: 100%;">
        <h3 style="letter-spacing: 1px;line-height: 1px;margin-top: 0;">
            <b style="color:#d46327;">( </b>Info Kajian Mingguan
        </h3>
    </div>
    <div class="container" style="margin-top: -15px;">
        <div class="row">
            <div class="rightSideAreaNews1" style="width:100%;">
            @foreach ($dataQueryKajianOneWeek as $dataAllKajianMingguan)
            <div class="block1">
                <div class="leftArea1">
                    <img src="{{ asset('public/storage/photo_info_kajian_mingguan/'.$dataAllKajianMingguan->photo) }}" class="img-thumbnail" style="margin: 0 auto; max-width:70px; margin-top: -7px;">
                </div>
                <div class="RightDetailArea1">
                    <div class="title1">{{ $dataAllKajianMingguan->nama }}</div>
                        <div class="descri1">
                            <p>
                            {{ date('d-m-Y', strtotime($dataAllKajianMingguan->tanggal)) }} / {{ \Carbon\Carbon::parse($dataAllKajianMingguan->tanggal)->formatLocalized('%A')}} / {{ substr($dataAllKajianMingguan->jam, 0, 5) }}
                            </p>
                        </div>
                    </div>
                </div>  
            @endforeach
            </div>
        </div>
    </div>
</div> 