@extends('backEND.layouts.index')

@section('page-content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                {{-- BREADCRUMD --}}
                <div class="page-header">
                    <div class="page-header-title">
                        <h4>Info Kajian Mingguan</h4>
                    </div>
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="index-2.html">
                                <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            {{-- <li class="breadcrumb-item"><a href="#!"></a></li> --}}
                            <li class="breadcrumb-item"><a href="#!">Info Kajian Mingguan</a></li>
                        </ul>
                    </div>
                </div>
                {{-- END BREADCRUMD --}}
                <div class="page-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Info Kajian Mingguan</h5>
                                    <div class="card-header-right">
                                        <a class="btn btn-primary" href="{{ URL('infokajianmingguan/create') }}">
                                            <i class="icofont icofont-ui-add"></i>
                                            Add Info Kajian Mingguan
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block">

                                    <table id="example" class="display" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Photo</th>
                                                <th>Tanggal</th>
                                                <th>Jam</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            @foreach ($m_info_kajian_mingguan as $key => $data)
                                            <tr>
                                                <td>{{ ($key+1) }}</td>
                                                <td>{{ $data->nama }}</td>
                                                <td>
                                                    <img src="{{ asset('public/storage/photo_info_kajian_mingguan/'.$data->photo) }}" class="img img-thumbnail" style="width: 100px;">
                                                </td>
                                                <td>{{ date('d-m-Y', strtotime($data->tanggal)) }}</td>
                                                <td>{{ $data->jam }}</td>
                                                <td>
                                                    <a href="{{ route('infokajianmingguan.edit', $data->id) }}" type="button" title="Edit" class="btn btn-warning btn-sm">
                                                        <i class="icofont icofont-pencil"></i>
                                                    </a>
                                                    
                                                    <form id="delete" action="{{ route('infokajianmingguan.destroy', $data->id) }}" 
                                                        method="POST" style="display: inline;">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                        <button title="Delete" class="btn btn-danger btn-sm"><i class="icofont icofont-trash"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                    

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- UNTUK SETTING COLOR -->
            <div id="styleSelector">
            </div>
        </div>
    </div>
</div>
                
@endsection

@section('js')
<script src="{{ asset('public/asset_admin/assets/js/datatables/js/datatables.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>

@endsection