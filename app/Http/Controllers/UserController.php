<?php

namespace App\Http\Controllers;

use Hash;
use Auth;
use Storage;
use View;

use App\User;
use App\MRoles;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['users'] = User::all();

        return view('backEND.User.view', $data);
    }

    
    public function create()
    {
        $getRole  = MRoles::all();

        // dd($getRole);

        return view('backEND.User.create')->with(['getRole' => $getRole]);
    }

  
    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $currentuserid      = Auth::id();
        $cekUser['users']   = User::all();
        $create_DateTime    = date("Y-m-d H:i:s");

        // # CEK VALIDATION FORM
        foreach($cekUser['users'] as $key => $data){
            if($data['name'] == $request->name){
                return redirect()->back()->with('name', 'Name already registered!');
            } else if($data['email'] == $request->email) {
                return redirect()->back()->with('email', 'Email already registered!');
            } else if($request->password != $request->password_confirmation) {
                return redirect()->back()->with('password', 'Please double-check the password and confirm the password, make sure they are the same!')->with('password_confirmation', 'Please double-check the password and confirm the password, make sure they are the same!'); 
            } else if($request->role == "" || $request->role == "null") {
                return redirect()->back()->with('role', 'Role cannot be empty!');
            }
        }
       
        if($request->photo == "null" || $request->photo == ""){
            $user_photo_user     = 'user.png';
            $path_photo_user     = 'storage/photo_user/user.png'; 
            
            $user = [
                'name'              => $request->name,
                'email'             => $request->email,
                'tempat_lahir'      => $request->tempat_lahir,
                'tanggal_lahir'     => date('Y-m-d', strtotime($request->tanggal_lahir)),
                'phone'             => $request->phone,
                'photo'             => $user_photo_user,
                'photo_path'        => $path_photo_user,
                'password'          => bcrypt($request->password),
                'role'              => $request->role,
                'created_by'        => $currentuserid,
                'created_at'        => $create_DateTime,
            ];

        } else {

            $uploadedFile   = $request->file('file');        
            $path           = $request->file('photo')->store('public/photo_user');
            $name_photo     = Str::substr(htmlspecialchars($path), 24, 80);
            $substrPath     = Str::substr($path, 6, 100);
            $pathDefault    = "storage".$substrPath;

            $user = [
                'name'              => $request->name,
                'email'             => $request->email,
                'tempat_lahir'      => $request->tempat_lahir,
                'tanggal_lahir'     => date('Y-m-d', strtotime($request->tanggal_lahir)),
                'phone'             => $request->phone,
                'photo'             => $name_photo,
                'photo_path'        => $pathDefault,
                'password'          => bcrypt($request->password),
                'role'              => $request->role,
                'created_by'        => $currentuserid,
                'created_at'        => $create_DateTime,
            ];

        }
        // dd($user);
        // die;  
        $save = User::insert($user);
        if($save)
            return redirect('users');
        else
            return redirect()->back()->withInput();

       
    }


    public function show($id)
    {
        //
    }

  
    public function edit($id)
    {
        $data['users'] = User::find($id);
        $getRole  = MRoles::all();
     
        return view('backEND.User.edit')->with(['data' => $data, 'getRole' => $getRole]);
    }

    
    public function update(Request $request, $id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $currentuserid      = Auth::id();
        $data['users']      = User::find($id);
        $create_DateTime    = date("Y-m-d H:i:s");
        $passwordNew        = Hash::make($request->password);

        
        // if kalo photo ktp dan photo user sama jangan di masukin array $request->ktp dan $request->photo 
        if($data['users']['password'] != $passwordNew) {
            $user = [
                'name'              => $request->name,
                'email'             => $request->email,
                'tempat_lahir'      => $request->tempat_lahir,
                'tanggal_lahir'     => date('Y-m-d', strtotime($request->tanggal_lahir)),
                //'jenis_kelamin'     => $request->jenis_kelamin,
                //'status'            => $request->status,
                //'pekerjaan'         => $request->pekerjaan,
                //'agama'             => $request->agama,
                'phone'             => $request->phone,
                'password'          => Hash::make($request->password),
                //'alamat_asal'       => $request->alamat_asal,
                //'status_rumah'      => $request->status_rumah,
                'role'              => $request->role,
                'update_by'         => $currentuserid,
                'updated_at'        => $create_DateTime,
            ];
            dd('0');
        } else {
            // tidak usah upload ktp dan photo user
            $user = [
                'name'              => $request->name,
                'email'             => $request->email,
                'tempat_lahir'      => $request->tempat_lahir,
                'tanggal_lahir'     => date('Y-m-d', strtotime($request->tanggal_lahir)),
                //'jenis_kelamin'     => $request->jenis_kelamin,
                //'status'            => $request->status,
                //'pekerjaan'         => $request->pekerjaan,
                'agama'             => $request->agama,
                'phone'             => $request->phone,
                //'alamat_asal'       => $request->alamat_asal,
                //'status_rumah'      => $request->status_rumah,
                'role'              => $request->role,
                'update_by'         => $currentuserid,
                'created_at'        => $create_DateTime,
            ];

            dd('1');
        } 

       dd('end');
        if($user){
            $msg = 'Your data has been successfully updated!';
        return redirect('users')->withSuccess($msg);
        } else {
            return redirect()->back()->withInput();
        }

    }

   
    public function destroy($id)
    {
        // dd('masuk');die;
        $users = User::find($id);
        if($users) {
            $users->destroy($id);
            $msg = 'Your data has been deleted!';
        } else {
            $msg = 'Please try again!';
        }
        return redirect()
            ->back()
            ->withSuccess($msg); 
    }


    public function UpdatePhotoEmpty($id){
        dd('masuk update ktp empty');
        $cekUser          = User::where('id', $id)->first();
        $delete_Photo_dir   = $cekUser['photo'];

        $filename = public_path().'/storage/photo_user/'.$cekUser['name'] .'/'.$delete_Photo_dir;
        File::delete($filename);

        // Set kosong pada field photo dan file path
        $emptyPhoto = [
            'photo' => "",
            'photo_path' => "",
        ];

        // update database user per id untuk field photo dan file path
        $updatePhoto = User::find($id)->update($emptyPhoto);
        if($updatePhoto){
            return redirect()->back()->withInput();
        }
    }
}
