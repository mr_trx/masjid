<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

setlocale(LC_TIME, 'id_ID');
\Carbon\Carbon::setLocale('id');
\Carbon\Carbon::now()->formatLocalized("%A, %d %B %Y");

class MInfoKajianMingguan extends Model
{
    protected $table='m_info_kajian_mingguan';
    protected $fillable = [
        'roles_user',
        'created_by'
    ];
}
