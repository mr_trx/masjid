<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MRoles extends Model
{
    protected $fillable = [
        'roles_user',
        'created_by'
    ];
}
