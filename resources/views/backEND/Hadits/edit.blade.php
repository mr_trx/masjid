@extends('backEND.layouts.index')

@section('page-content')

<div class="pcoded-content">
        <div class="pcoded-inner-content">
           <div class="main-body">
              <div class="page-wrapper">
                 {{-- BREADCRUMD --}}
                 <div class="page-header">
                    <div class="page-header-title">
                       <h4>Edit Hadits - Hadits</h4>
                    </div>
                    <div class="page-header-breadcrumb">
                       <ul class="breadcrumb-title">
                          <li class="breadcrumb-item">
                             <a href="index-2.html">
                             <i class="icofont icofont-home"></i>
                             </a>
                          </li>
                          <li class="breadcrumb-item"><a href="#!">Hadits - Hadits</a></li>
                          <li class="breadcrumb-item"><a href="#!">Edit Hadits - Hadits</a></li>
                       </ul>
                    </div>
                 </div>
                 {{-- END BREADCRUMD --}}
                 <div class="page-body">
                    <div class="row">
                       <div class="col-lg-12">
                          <div class="card">
                             <div class="card-header">
                                <h5>Edit Hadits - Hadits</h5>
                             </div>
                             <div class="card-block">
                                <form id="main" method="post" action="{{ URL::to('hadits',$data['m_hadits']->id)}}" enctype="multipart/form-data" id="myReset">
                                {{ csrf_field() }}
                                {{ method_field('put') }}
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Konten Hadits</label>
                                        <div class="col-sm-10">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="file" id="image-input" style="display: none;">
                                            <textarea rows="5" cols="5" class="form-control form-control-danger" name="konten">{{ isset($data['m_hadits']) ? $data['m_hadits']->konten : '' }}</textarea>
                                        </div>
                                    </div>
                                   
                                    <div class="form-group row">
                                        <label class="col-sm-2"></label>
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-danger m-b-0">Reset</button>
                                            <button type="submit" class="btn btn-primary m-b-0">Update</button>
                                        </div>
                                    </div>
                                </form>
            
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </div>
              <!-- UNTUK SETTING COLOR -->
              <div id="styleSelector"></div>
           </div>
        </div>
     </div>

@endsection

@section('js')
<script src="{{ asset('public/vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('public/vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
<script>
    $('textarea').ckeditor();
    // $('.textarea').ckeditor(); // if class is prefered.
</script>
@endsection