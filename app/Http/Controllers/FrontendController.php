<?php

namespace App\Http\Controllers;

use Hash;
use Auth;
use File;
use Storage;
use View;

use Carbon\Carbon;
use App\User;
use App\MInfoProgramMasjids;
use App\MInfoKajianMingguan;
use App\MInfoSholatJumat;
use App\MHadits;
use App\MWaktuAzan;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class FrontendController extends Controller
{
    public function index()
    {
        $thismonth              = date('m');
        $dataProgram            = MInfoProgramMasjids::get();
        $dataKajian             = MInfoKajianMingguan::get();
        $dataHadits             = MHadits::get();
        $dataQueryKajianOneWeek = DB::select(DB::raw('SELECT * FROM m_info_kajian_mingguan where tanggal > now() - interval 1 week;'));
        $dataWaktuAzan          = MWaktuAzan::get();
        // $dataSholat             = MInfoSholatJumat::whereMonth('tanggal', '=', $thismonth)->get();
        $dataSholat             = MInfoSholatJumat::get();
 
        return view('welcome')->with([
                                        'dataProgram' => $dataProgram, 
                                        'dataKajian' => $dataKajian, 
                                        'dataSholat' => $dataSholat, 
                                        'dataHadits' => $dataHadits,
                                        'dataQueryKajianOneWeek' => $dataQueryKajianOneWeek,
                                        'dataWaktuAzan' => $dataWaktuAzan
                                    ]);
    }


    public function getWaktuAzan(){
        $waktuAzan = MWaktuAzan::get();
        
        return json_encode(array('data'=>$waktuAzan));
    }

    
}
