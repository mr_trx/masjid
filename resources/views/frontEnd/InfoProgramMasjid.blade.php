<div class="col-md-5" style="height:100%">
    <!-- <div class="prokes" style="display:none;">
        <img class="img-responsive" 
        style="margin: 0 auto; max-width:688px;margin-top:-16px;height:377px;margin-left: -15px;"
            src="{{URL::asset('public/image/prokes.png')}}" alt="">
    </div> -->
    <div class="programmasjid">
        <div class="text-center" style="padding: 5px;margin: 0 auto;width: 100%;">
            <h2 style="letter-spacing: 1px;line-height:1px;">
                <b style="color:#d46327;">( </b>Info Program Masjid Baitul Ma'mur
            </h2>
        </div>
        <div class="container" style="margin-top: -114px;">
            <div class="row">
                <div class="">
                    
                    <div class="rightSideAreaNews">
                        @foreach ($dataProgram as $dataAllProgramMasjid)
                        <div class="block">
                            <div class="leftArea">
                                <div class="dateTime">
                                    <!-- untuk penempatan div waktu tanggal dan teks -->
                                    @if($dataAllProgramMasjid->pilih_waktu == 'tanggal' || $dataAllProgramMasjid->pilih_waktu == '')
                                        <span class="date">{{ date('d', strtotime($dataAllProgramMasjid->tanggal)) }}</span>
                                        <span class="month-year">{{ \Carbon\Carbon::parse($dataAllProgramMasjid->tanggal)->format('M')}}&nbsp;{{ date('Y', strtotime($dataAllProgramMasjid->tanggal)) }}</span>
                                    @elseif($dataAllProgramMasjid->pilih_waktu == 'teks')
                                          <span class="date" style="font-size: 11px;line-height: 14.2px;text-align: center;"> {{ $dataAllProgramMasjid->waktu_teks }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="RightDetailArea">
                                <div class="title" style="font-size:25px;">{{ $dataAllProgramMasjid->judul }}</div>
                                <div class="descri" style="font-size:20px;">
                                    <p>{!! $dataAllProgramMasjid->konten !!}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach 
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>