<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MInfoProgramMasjids extends Model
{
    protected $fillable = [
        'roles_user',
        'created_by'
    ];
}
