<div class="col-md-5" style="height:100%;max-height:352px;">
    <!-- DIBUAT SLIDESHOW -->
    <div class="container">
        <div id="slider">
            <a href="#" class="next" style="display:none">></a> 	
            <a href="#" class="prev" style="display:none"><</a>
            <ul>
                @foreach ($dataHadits as $dataAllHadits)
                    <li>
                        {!! $dataAllHadits->konten !!}
                    </li>
                @endforeach
            </ul>
        </div>
        <script>
        $("#slider").sliderNd({
            delay		:	1000,
            autoplay	:	true,
            navigation	:	true
        });
        </script>
    </div>
</div> 