-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.1.19-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for masjid
CREATE DATABASE IF NOT EXISTS `masjid` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `masjid`;

-- Dumping structure for table masjid.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table masjid.migrations: ~10 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2021_08_08_032259_create_MRoles_table', 2),
	(4, '2021_08_08_033927_add_column_to_users_table', 3),
	(5, '2021_08_08_034820_add_columnPhone_to_users_table', 4),
	(6, '2021_08_15_042254_create_M_Info_Program_Masjids_table', 5),
	(7, '2021_08_15_044729_create_M_Info_Sholat_Jumat_table', 6),
	(8, '2021_08_15_053900_create_M_Info_Kajian_Mingguan_table', 7),
	(9, '2021_09_05_041944_create_M_Hadits_table', 8),
	(10, '2021_09_08_152222_create_M_Waktu_Azan_table', 9),
	(11, '2021_09_08_152258_create_M_Waktu_Iqomah_table', 9);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table masjid.m_hadits
CREATE TABLE IF NOT EXISTS `m_hadits` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `konten` text COLLATE utf8_unicode_ci NOT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table masjid.m_hadits: ~6 rows (approximately)
/*!40000 ALTER TABLE `m_hadits` DISABLE KEYS */;
INSERT INTO `m_hadits` (`id`, `konten`, `insert_by`, `update_by`, `created_at`, `updated_at`) VALUES
	(1, '<p>Ø³ÙŽÙˆÙÙ‘ÙˆØ§ ØµÙÙÙÙˆÙÙŽÙƒÙÙ…Ù’ ÙÙŽØ¥ÙÙ†ÙŽÙ‘ ØªÙŽØ³Ù’ÙˆÙÙŠÙŽØ©ÙŽ Ø§Ù„ØµÙŽÙ‘ÙÙÙ‘ Ù…ÙÙ†Ù’ ØªÙŽÙ…ÙŽØ§Ù…Ù Ø§Ù„ØµÙŽÙ‘Ù„Ø§ÙŽØ©Ù<br />\r\n&quot;Luruskanlah shaf karena lurusnya shaf merupakan bagian dari kesempurnaan shalat.&quot;(HR. Bukhari no. 723 dan Muslim no. 433)&nbsp;</p>', 5, 5, '2021-09-05 12:13:00', '2021-10-24 18:54:53'),
	(3, '<p>Ø¥ÙÙ†ÙŽÙ‘Ù…ÙŽØ§ Ø§Ù„ØµÙŽÙ‘Ø¨Ù’Ø±Ù Ø¹ÙÙ†Ù’Ø¯ÙŽ Ø§Ù„ØµÙŽÙ‘Ø¯Ù’Ù…ÙŽØ©Ù Ø§Ù„Ø£ÙÙˆÙ„ÙŽÙ‰<br />\r\n&quot;Sesungguhnya sabar adalah ketika diawal musibah.&quot;(HR. Bukhari)&nbsp;</p>', 5, 5, '2021-09-05 15:53:01', '2021-10-24 18:54:35'),
	(4, '<p>Ø§Ù„Ø¯Ù‘ÙÙ†Ù’ÙŠÙŽØ§ Ø³ÙØ¬Ù’Ù†Ù Ø§Ù„Ù’Ù…ÙØ¤Ù’Ù…ÙÙ†Ù ÙˆÙŽØ¬ÙŽÙ†Ù‘ÙŽØ©Ù Ø§Ù„Ù’ÙƒÙŽØ§ÙÙØ±Ù<br />\r\n&quot;Dunia adalah penjara bagi orang beriman dan surga bagi orang kafir.&quot;&nbsp;(H.R. Muslim)</p>', 5, 5, '2021-09-05 15:54:51', '2021-10-24 18:55:14'),
	(5, '<p>ÙˆÙŽØ§Ù„Ù„Ù‘ÙŽÙ‡Ù ÙÙÙŠ Ø¹ÙŽÙˆÙ’Ù†Ù Ø§Ù„Ù’Ø¹ÙŽØ¨Ù’Ø¯Ù Ù…ÙŽØ§ ÙƒÙŽØ§Ù†ÙŽ Ø§Ù„Ù’Ø¹ÙŽØ¨Ù’Ø¯Ù ÙÙÙŠ Ø¹ÙŽÙˆÙ’Ù†Ù Ø£ÙŽØ®ÙÙŠÙ‡Ù<br />\r\n&quot;Allah senantiasa menolong hamba selama ia menolong saudaranya.&quot;&nbsp;(H.R. Muslim)</p>', 5, 5, '2021-10-24 01:12:32', '2021-10-24 18:55:32'),
	(6, '<p>Ø®ÙŽÙŠÙ’Ø±ÙÙƒÙÙ…Ù’ Ù…ÙŽÙ†Ù’ ØªÙŽØ¹ÙŽÙ„Ù‘ÙŽÙ…ÙŽ Ø§Ù„Ù’Ù‚ÙØ±Ù’Ø¢Ù†ÙŽ ÙˆÙŽØ¹ÙŽÙ„Ù‘ÙŽÙ…ÙŽÙ‡Ù<br />\r\n&quot;Sebaik-baik diantara kalian adalah yang mempelajari al-Quran dan mengajarkannya.&quot;(HR. Bukhari)&nbsp;</p>', 5, 5, '2021-10-24 01:16:49', '2021-10-24 18:55:59'),
	(7, '<p>Ø£ÙŽÙÙ’Ø´ÙÙˆØ§ Ø§Ù„Ø³Ù‘ÙŽÙ„Ø§ÙŽÙ…ÙŽ Ø¨ÙŽÙŠÙ’Ù†ÙŽÙƒÙÙ…Ù’ ØªÙŽØ­ÙŽØ§Ø¨Ù‘ÙÙˆØ§<br />\r\n&quot;Sebarkanlah salam diantara kalian, niscaya kalian akan saling mencintai&quot;(HR. Al-Hakim | Shahih | Shahih Al Jaami No.1086)&nbsp;</p>', 5, NULL, '2021-10-24 18:56:18', NULL);
/*!40000 ALTER TABLE `m_hadits` ENABLE KEYS */;

-- Dumping structure for table masjid.m_info_kajian_mingguan
CREATE TABLE IF NOT EXISTS `m_info_kajian_mingguan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal` date DEFAULT NULL,
  `jam` time NOT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table masjid.m_info_kajian_mingguan: ~5 rows (approximately)
/*!40000 ALTER TABLE `m_info_kajian_mingguan` DISABLE KEYS */;
INSERT INTO `m_info_kajian_mingguan` (`id`, `nama`, `photo`, `photo_path`, `tanggal`, `jam`, `insert_by`, `update_by`, `created_at`, `updated_at`) VALUES
	(1, 'Kajian Kitab Tauhid / Syaikh Muhammad At-Tamimi / Ustadz Yovin Abu Hammam', 'user_profile.png', 'storage/photo_info_kajian_mingguan/user_profile.png', '2021-11-04', '18:15:00', 5, 5, '2021-08-30 08:38:29', '2021-10-24 21:34:14'),
	(2, 'Kajian Kitab Bulughul Maram / Ibnu Hajar As-Qalani / Ustadz Abu Yaâ€™la Hizbul Majid', 'user_profile.png', 'storage/photo_info_kajian_mingguan/user_profile.png', '2021-11-11', '18:15:00', 5, 5, '2021-08-30 08:38:29', '2021-10-24 21:34:06'),
	(3, 'Kajian Kitab Tauhid / Syaikh Muhammad At-Tamimi / Ustadz Yovin Abu Hammam', 'user_profile.png', 'storage/photo_info_kajian_mingguan/user_profile.png', '2021-11-18', '18:15:00', 5, 5, '2021-09-05 13:03:07', '2021-10-24 21:33:49'),
	(4, 'Kajian Kitab Bulughul Maram / Ibnu Hajar As-Qalani / Ustadz Abu Yaâ€™la Hizbul Majid', 'user_profile.png', 'storage/photo_info_kajian_mingguan/user_profile.png', '2021-11-25', '18:15:00', 5, 5, '2021-09-05 13:04:08', '2021-10-24 21:33:58'),
	(5, 'Kajian Tematik / Kuliah Subuh Setiap Ahad', 'user_profile.png', 'storage/photo_info_kajian_mingguan/user_profile.png', '2021-10-24', '05:00:00', 5, 5, '2021-10-24 20:38:50', '2021-10-24 20:39:09');
/*!40000 ALTER TABLE `m_info_kajian_mingguan` ENABLE KEYS */;

-- Dumping structure for table masjid.m_info_program_masjids
CREATE TABLE IF NOT EXISTS `m_info_program_masjids` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pilih_waktu` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `waktu_teks` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `judul` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `konten` text COLLATE utf8_unicode_ci NOT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table masjid.m_info_program_masjids: ~7 rows (approximately)
/*!40000 ALTER TABLE `m_info_program_masjids` DISABLE KEYS */;
INSERT INTO `m_info_program_masjids` (`id`, `pilih_waktu`, `tanggal`, `waktu_teks`, `judul`, `konten`, `insert_by`, `update_by`, `created_at`, `updated_at`) VALUES
	(2, NULL, '2021-10-25', NULL, 'Pengajian Anak-anak (Iqro)', '<p><strong>Setiap hari Senin &ndash; Jum&rsquo;at</strong></p>\r\n\r\n<p>Waktu: Ba&rsquo;da Maghrib sampai dengan selesai</p>', 5, 5, '2021-08-29 11:36:23', '2021-10-25 21:38:49'),
	(3, NULL, '2021-10-25', NULL, 'Pengajian Akhwat', '<p>Setiap hari Senin (Pimpinan Ibu Eranawan)</p>\r\n\r\n<p>Waktu: Jam 10.00 sampai dengan selesai</p>', 5, 5, '2021-08-29 11:45:21', '2021-10-24 21:00:50'),
	(4, NULL, '2021-10-26', NULL, 'Pengajian Akhwat', '<p>Setiap hari Selasa (Pimpinan Ibu Yuni Ridwan)</p>\r\n\r\n<p>Waktu: Jam 10.00 sampai dengan selesai</p>', 5, 5, '2021-08-29 13:33:15', '2021-10-24 21:02:26'),
	(5, NULL, '2021-10-27', NULL, 'Pengajian Akhwat', '<p>Setiap hari Rabu (Pimpinan Ibu Wisaksono)</p>\r\n\r\n<p>Waktu: Jam 10.00 sampai dengan selesai</p>', 5, 5, '2021-08-30 10:28:34', '2021-10-24 21:02:50'),
	(6, NULL, '2021-10-24', NULL, 'Tahsin Qurâ€™an Metode Utsmani', '<p>Sesuai jadwal kelas (Pimpinan Ustadz Muksin Sanusi)</p>', 5, NULL, '2021-10-24 21:04:15', NULL);
/*!40000 ALTER TABLE `m_info_program_masjids` ENABLE KEYS */;

-- Dumping structure for table masjid.m_info_sholat_jumat
CREATE TABLE IF NOT EXISTS `m_info_sholat_jumat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sebagai` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table masjid.m_info_sholat_jumat: ~7 rows (approximately)
/*!40000 ALTER TABLE `m_info_sholat_jumat` DISABLE KEYS */;
INSERT INTO `m_info_sholat_jumat` (`id`, `nama`, `photo`, `photo_path`, `sebagai`, `tanggal`, `insert_by`, `update_by`, `created_at`, `updated_at`) VALUES
	(3, 'Ustadz Isnan Anshory, MA', 'KZANBsMG3SgjjEDv5cgAVqwLXwdzIcsnNEfPfUrT.jpeg', 'storage/photo_info_sholat_jumat/KZANBsMG3SgjjEDv5cgAVqwLXwdzIcsnNEfPfUrT.jpeg', 'Khatib', '2021-08-20', 5, 5, '2021-08-29 20:51:29', '2021-10-24 21:25:38'),
	(4, 'Ustadz Mahmudin, S.ThI', 'UgtUAELWbQkyPYWSigenJYa6i9jzMprl3tUZjQ5z.jpeg', 'storage/photo_info_sholat_jumat/UgtUAELWbQkyPYWSigenJYa6i9jzMprl3tUZjQ5z.jpeg', 'Khatib', '2021-08-27', 5, 5, '2021-08-29 21:04:50', '2021-10-24 21:26:42'),
	(5, 'Ustadz Achmad Ridwan, SE. MM.', 'epwU7T65KDcVsxXwoLRQsM8Y96qgUn0pUvLgOqKe.jpeg', 'storage/photo_info_sholat_jumat/epwU7T65KDcVsxXwoLRQsM8Y96qgUn0pUvLgOqKe.jpeg', 'Khatib', '2021-09-03', 5, 5, '2021-09-05 13:00:52', '2021-10-24 21:27:33'),
	(6, 'Ustadz Moh Toharuddin Al-Hafidz', 'soZE4vBHAR2thNrBXDLgk5O7LsYvUqFMXcz0QcSu.jpeg', 'storage/photo_info_sholat_jumat/soZE4vBHAR2thNrBXDLgk5O7LsYvUqFMXcz0QcSu.jpeg', 'Khatib', '2021-09-10', 5, 5, '2021-09-05 13:02:14', '2021-10-24 21:28:26'),
	(7, 'Ustadz Dr. Syafaruddin Tanjung, MPd', 'LUpCNRJgi6bP3t3bFCXCEgFChSEyGrb1LWsgxnsw.jpeg', 'storage/photo_info_sholat_jumat/LUpCNRJgi6bP3t3bFCXCEgFChSEyGrb1LWsgxnsw.jpeg', 'Khatib', '2021-09-17', 5, 5, '2021-09-05 18:53:17', '2021-10-24 21:29:11'),
	(8, 'Ustadz Syihabuddin Umar, SE. MM', 'zLKBgkRbpn7NsAbqmAjJGGxodH0lsTpp69E6mUko.jpeg', 'storage/photo_info_sholat_jumat/zLKBgkRbpn7NsAbqmAjJGGxodH0lsTpp69E6mUko.jpeg', 'Khatib', '2021-09-24', 5, 5, '2021-09-05 18:53:43', '2021-10-24 21:29:48'),
	(9, 'Ustadz Mufti Syafi\'i Lc Al-Hafidz', 'GGyDk1gvrMeGTDLH7wJ6ztOT14a2pqwPvBocHt6I.jpeg', 'storage/photo_info_sholat_jumat/GGyDk1gvrMeGTDLH7wJ6ztOT14a2pqwPvBocHt6I.jpeg', 'Khatib', '2021-10-01', 5, 5, '2021-09-05 18:54:00', '2021-10-24 21:30:34'),
	(10, 'Ustadz Sugiarto', 'user_profile.png', 'storage/photo_info_sholat_jumat/user_profile.png', 'Khatib', '2021-10-20', 5, 5, '2021-09-05 18:54:14', '2021-10-24 21:31:18');
/*!40000 ALTER TABLE `m_info_sholat_jumat` ENABLE KEYS */;

-- Dumping structure for table masjid.m_roles
CREATE TABLE IF NOT EXISTS `m_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `roles_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table masjid.m_roles: ~0 rows (approximately)
/*!40000 ALTER TABLE `m_roles` DISABLE KEYS */;
INSERT INTO `m_roles` (`id`, `roles_user`, `created_by`, `update_by`, `created_at`, `updated_at`) VALUES
	(1, 'Administrator', 1, 0, '2021-08-08 10:32:05', NULL);
/*!40000 ALTER TABLE `m_roles` ENABLE KEYS */;

-- Dumping structure for table masjid.m_waktu_azan
CREATE TABLE IF NOT EXISTS `m_waktu_azan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `waktu` int(11) NOT NULL DEFAULT '0',
  `insert_by` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table masjid.m_waktu_azan: ~0 rows (approximately)
/*!40000 ALTER TABLE `m_waktu_azan` DISABLE KEYS */;
INSERT INTO `m_waktu_azan` (`id`, `waktu`, `insert_by`, `update_by`, `created_at`, `updated_at`) VALUES
	(2, 50, 5, NULL, '2021-10-18 00:26:52', NULL);
/*!40000 ALTER TABLE `m_waktu_azan` ENABLE KEYS */;

-- Dumping structure for table masjid.m_waktu_iqomah
CREATE TABLE IF NOT EXISTS `m_waktu_iqomah` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `waktu` int(11) NOT NULL DEFAULT '0',
  `insert_by` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table masjid.m_waktu_iqomah: ~1 rows (approximately)
/*!40000 ALTER TABLE `m_waktu_iqomah` DISABLE KEYS */;
INSERT INTO `m_waktu_iqomah` (`id`, `waktu`, `insert_by`, `update_by`, `created_at`, `updated_at`) VALUES
	(1, 600, 5, 5, '2021-10-16 23:25:25', '2021-10-21 09:28:46');
/*!40000 ALTER TABLE `m_waktu_iqomah` ENABLE KEYS */;

-- Dumping structure for table masjid.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table masjid.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table masjid.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tempat_lahir` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table masjid.users: ~1 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `tempat_lahir`, `tanggal_lahir`, `phone`, `photo`, `photo_path`, `role`, `created_by`, `update_by`, `created_at`, `updated_at`) VALUES
	(4, 'Hafizd Arrohman', 'hafizdarrohman@gmail.com', '$2y$10$KvYuixJ3ushLYnEFp2rPJ.Dtv/PDJVJYAp0qqnXFLgFBhWb4uw4wG', 'ZSX9HVsRhFo8C6lUu96by2xKTipJRGuvheM1lph47qX6sYlyYk04ab1T3bMy', 'Jakarta', '1994-04-25', '089666839398', 'UR5rb9HqcaWE7hoc2rFkwxxAn17INPaeIf.png', 'storage/photo_user/m7nUVPUR5rb9HqcaWE7hoc2rFkwxxAn17INPaeIf.png', 1, 2, NULL, '2021-08-15 12:32:32', NULL),
	(5, 'Tri Busono', 'tribusono22@gmail.com', '$2y$10$HGxi65HawnCtdL3t9DcdoOciNnkQM3aIIE/GV6rCRaLqaZhPX4JbG', 'bOZJmq2BJGxGn2HSGqhkYCelNv8oHL9ENSZHW5MknFqAuxAQtj29CrWMwTUA', 'Jakarta', '1993-11-22', '081293395780', 'BEnpGmIbzJuzN91GAfuSRW3tP8tj7sQMNP.png', 'storage/photo_user/3NMH4ABEnpGmIbzJuzN91GAfuSRW3tP8tj7sQMNP.png', 1, 4, NULL, '2021-08-15 12:36:35', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
