@extends('backEND.layouts.index')

@section('page-content')

<div class="pcoded-content">
        <div class="pcoded-inner-content">
           <div class="main-body">
              <div class="page-wrapper">
                 {{-- BREADCRUMD --}}
                 <div class="page-header">
                    <div class="page-header-title">
                       <h4>Create Info Sholat Jum'at</h4>
                    </div>
                    <div class="page-header-breadcrumb">
                       <ul class="breadcrumb-title">
                          <li class="breadcrumb-item">
                             <a href="index-2.html">
                             <i class="icofont icofont-home"></i>
                             </a>
                          </li>
                          <li class="breadcrumb-item"><a href="#!">Info Sholat Jum'at</a></li>
                          <li class="breadcrumb-item"><a href="#!">Create Info Sholat Jum'at</a></li>
                       </ul>
                    </div>
                 </div>
                 {{-- END BREADCRUMD --}}
                 <div class="page-body">
                    <div class="row">
                       <div class="col-lg-12">
                          <div class="card">
                             <div class="card-header">
                                <h5>Create Info Sholat Jum'at</h5>
                             </div>
                             <div class="card-block">
                                <form id="main" method="post" action="{{ URL::to('infosholatjumat',$data['m_info_sholat_jumat']->id)}}" enctype="multipart/form-data" id="myReset">
                                {{ csrf_field() }}
                                {{ method_field('put') }}
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Nama</label>
                                        <div class="col-sm-10">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input 
                                            type="text" 
                                            class="form-control form-control-danger" 
                                            name="nama" 
                                            value="{{ isset($data['m_info_sholat_jumat']) ? $data['m_info_sholat_jumat']->nama : '' }}"
                                            required
                                            >
                                            <!-- @if (session()->has('name'))
                                            <span class="messages">*  {{ session('name') }}</span>
                                            @endif -->
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Photo</label>
                                        <div class="col-sm-5">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            @if($data['m_info_sholat_jumat']->photo == "" || $data['m_info_sholat_jumat']->photo == null)
                                                <input 
                                                    type="file" 
                                                    name="photo" 
                                                    class="form-control"
                                                >
                                            @else
                                                <div class="pull-right">
                                                    <a href="{{ URL('UpdatePhotoEmpty/' . $data['m_info_sholat_jumat']->id) }}" title="Delete Photo">
                                                        <img src="{{ URL::asset('public/asset_admin/image/close.png') }}" style="position: absolute;width: 35px;margin-top: 1px;margin-left: -115px;box-shadow: 0 0 25px -2px #b7b7b7;">
                                                    </a>
                                                </div>
                                                <input type="hidden" name="photo" value="{{ isset($data['m_info_sholat_jumat']) ? $data['m_info_sholat_jumat']->photo : '' }}" >
                                                <img src="{{ asset('public/'.$data['m_info_sholat_jumat']->photo_path) }}" alt=""  style="border: 1px solid #fff;width: 300px;">
                                            @endif
                                            <!-- @if (session()->has('photo'))
                                            <span class="messages">* {{ session('photo') }}</span>     
                                            @endif -->
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Sebagai</label>
                                        <div class="col-sm-10">
                                            <select name="sebagai" class="form-control">
                                                <option value="
                                                @if($data['m_info_sholat_jumat']->sebagai == null || $data['m_info_sholat_jumat']->sebagai == '')
                                                    selected
                                                @endif"
                                                ></option>

                                                @foreach($status as $key => $sebagai)
                                                    <option value="{{ $sebagai }}"  
                                                        @if($sebagai == $data['m_info_sholat_jumat']->sebagai){
                                                            selected
                                                        }
                                                        @endif
                                                    >{{ $sebagai }}</option>
                                                @endforeach
                                            </select>
                                            <!-- @if (session()->has('sebagai'))
                                            <span class="messages">* {{ session('sebagai') }}</span>     
                                            @endif -->
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Tanggal</label>
                                        <div class="col-sm-5">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input 
                                            id="dropper-default" 
                                            class="form-control form-control-danger" 
                                            type="text" 
                                            name="tanggal" 
                                            value="{{ isset($data['m_info_sholat_jumat']) ? $data['m_info_sholat_jumat']->tanggal : '' }}"
                                            required
                                            >
                                        </div>
                                    </div>
                            
                                    <div class="form-group row">
                                        <label class="col-sm-2"></label>
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-danger m-b-0">Reset</button>
                                            <button type="submit" class="btn btn-primary m-b-0">Update</button>
                                        </div>
                                    </div>
                                </form>
            
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </div>
              <!-- UNTUK SETTING COLOR -->
              <div id="styleSelector"></div>
           </div>
        </div>
     </div>

@endsection

@section('js')
<script src="{{ asset('public/vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('public/vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/asset_admin/bower_components/datedropper/datedropper.min.js') }}"></script>
<script>
   $('#dropper-default').dateDropper({
      theme: 'leaf'
   });
</script> 
<script>
    $('textarea').ckeditor();
    // $('.textarea').ckeditor(); // if class is prefered.
</script>
@endsection