<?php

namespace App\Http\Controllers;

use Hash;
use Auth;
use File;
use Storage;
use View;

use App\MInfoSholatJumat;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class InfoSholatJumatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['m_info_sholat_jumat'] = MInfoSholatJumat::all();

        return view('backEND.InfoSholatJumat.view', $data);
    }

    
    public function create()
    {
        $status = [
            '1'     => 'Khatib',
            '2'     => 'Muadzin',
        ];

        return view('backEND.InfoSholatJumat.create')->with(['status' => $status]);
    }

    
    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $currentuserid  = Auth::id();
        $today          = date("Y-m-d H:i:s"); 

        if($request->photo == "null" || $request->photo == ""){

            $user_photo_user     = 'user_profile.png';
            $path_photo_user     = 'storage/photo_info_sholat_jumat/user_profile.png'; 

            $all_data_sholat = [
                'nama'          => $request->nama,
                'photo'         => $user_photo_user,
                'photo_path'    => $path_photo_user,
                'sebagai'       => $request->sebagai,
                'tanggal'       => date('Y-m-d', strtotime($request->tanggal)),
                'insert_by'     => $currentuserid,
                'created_at'    =>  $today,
            ];
        } else {

            $uploadedFile   = $request->file('file');        
            $path           = $request->file('photo')->store('public/photo_info_sholat_jumat');
            $name_photo     = Str::substr(htmlspecialchars($path), 31, 87);
            $substrPath     = Str::substr($path, 6, 100);
            $pathDefault    = "storage".$substrPath;

            $all_data_sholat = [
                'nama'          => $request->nama,
                'photo'         => $name_photo,
                'photo_path'    => $pathDefault,
                'sebagai'       => $request->sebagai,
                'tanggal'       => date('Y-m-d', strtotime($request->tanggal)),
                'insert_by'     => $currentuserid,
                'created_at'    =>  $today,
            ];
        }

        //dd($all_data_sholat);die;
    
        $save = MInfoSholatJumat::insert($all_data_sholat);
        if($save)
            return redirect('infosholatjumat');
        else
            return redirect()->back()->withInput();


    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        $data['m_info_sholat_jumat'] = MInfoSholatJumat::find($id);
        $status = [
            '1'     => 'Khatib',
            '2'     => 'Muadzin',
        ];
     
        return view('backEND.InfoSholatJumat.edit')->with(['data' => $data, 'status' => $status]);
    }

   
    public function update(Request $request, $id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $currentuserid  = Auth::id();
        $today          = date("Y-m-d H:i:s"); 
        $cekData        = MInfoSholatJumat::where('id', $id)->first(); 
        //dd($request->photo);

        if($request->photo == "null" || $request->photo == ""){
            //dd('masuk 1');
            $user_photo_user     = 'user_profile.png';
            $path_photo_user     = 'storage/photo_info_sholat_jumat/user_profile.png'; 

            $update_data_sholat = [
                'nama'          => $request->nama,
                'photo'         => $user_photo_user,
                'photo_path'    => $path_photo_user,
                'sebagai'       => $request->sebagai,
                'tanggal'       => date('Y-m-d', strtotime($request->tanggal)),
                'update_by'     => $currentuserid,
                'updated_at'    => $today,
            ];

        } else if($cekData['photo'] == $request->photo) {
            //dd('masuk 2');
            $update_data_sholat = [
                'nama'          => $request->nama,
                'sebagai'       => $request->sebagai,
                'tanggal'       => date('Y-m-d', strtotime($request->tanggal)),
                'update_by'     => $currentuserid,
                'updated_at'    => $today,
            ];
            
        } else {
            //dd('masuk 3');
            $uploadedFile   = $request->file('file');        
            $path           = $request->file('photo')->store('public/photo_info_sholat_jumat');
            $name_photo     = Str::substr(htmlspecialchars($path), 31, 87);
            $substrPath     = Str::substr($path, 6, 100);
            $pathDefault    = "storage".$substrPath;

            $update_data_sholat = [
                'nama'          => $request->nama,
                'photo'         => $name_photo,
                'photo_path'    => $pathDefault,
                'sebagai'       => $request->sebagai,
                'tanggal'       => date('Y-m-d', strtotime($request->tanggal)),
                'update_by'     => $currentuserid,
                'updated_at'    => $today,
            ];
        }

        //dd($update_data_sholat); die;
        $updatedata = MInfoSholatJumat::where('id',$id)->update($update_data_sholat);
        if($updatedata){
            $msg = 'Your data has been successfully updated!';
        return redirect('infosholatjumat')->withSuccess($msg);
        } else {
            return redirect()->back()->withInput();
        }
    }

    
    public function destroy($id)
    {
        //dd($id);die;
        $infoSholat = MInfoSholatJumat::find($id);
        $cekData    = MInfoSholatJumat::where('id', $id)->first();
        
        if($cekData['photo'] == 'user_profile.png'){
            if($infoSholat) {
                $infoSholat->destroy($id);
                $msg = 'Your data has been deleted!';
            } else {
                $msg = 'Please try again!';
            }
            return redirect()
                ->back()
                ->withSuccess($msg); 

        } else {
            $delete_Photo_dir   = $cekData['photo'];

            $filename = public_path().'/storage/photo_info_sholat_jumat/'.$delete_Photo_dir;
            File::delete($filename);

            if($infoSholat) {
                $infoSholat->destroy($id);
                $msg = 'Your data has been deleted!';
            } else {
                $msg = 'Please try again!';
            }
            return redirect()
                ->back()
                ->withSuccess($msg); 
        }
    }

    public function UpdatePhotoEmpty($id){
        //dd('masuk update Photo');die;
        $cekData          = MInfoSholatJumat::where('id', $id)->first();
        if($cekData['photo'] == 'user_profile.png'){
            // dd('tidak bisa dihapus');
            // Set kosong pada field photo dan file path
            $emptyPhoto = [
                'photo' => "",
                'photo_path' => "",
            ];

            // update database user per id untuk field photo dan file path
            $updatePhoto = MInfoSholatJumat::where('id',$id)->update($emptyPhoto);
            if($updatePhoto){
                return redirect()->back()->withInput();
            }
        } else {
            //dd('berhasil dihapus');
            $delete_Photo_dir   = $cekData['photo'];

            $filename = public_path().'/storage/photo_info_sholat_jumat/'.$delete_Photo_dir;
            File::delete($filename);

            // Set kosong pada field photo dan file path
            $emptyPhoto = [
                'photo' => "",
                'photo_path' => "",
            ];

            // update database user per id untuk field photo dan file path
            $updatePhoto = MInfoSholatJumat::where('id',$id)->update($emptyPhoto);
            if($updatePhoto){
                return redirect()->back()->withInput();
            }
        }
        
    }
}
