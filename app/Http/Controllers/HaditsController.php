<?php

namespace App\Http\Controllers;

use Hash;
use Auth;
use Storage;
use View;

use App\User;
use App\MHadits;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class HaditsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $data['m_hadits'] = MHadits::all();

        return view('backEND.Hadits.view', $data);
    }

    
    public function create()
    {
        return view('backEND.Hadits.create');
    }


    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $userID      = Auth::id();
        $today       = date("Y-m-d H:i:s"); 

        $all_data_hadits = [
            'konten'        => $request->konten,
            'insert_by'     => $userID,
            'created_at'    => $today,
        ];
    
        $save = MHadits::insert($all_data_hadits);
        if($save)
            return redirect('hadits');
        else
            return redirect()->back()->withInput();
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        $data['m_hadits'] = MHadits::find($id);
     
        return view('backEND.Hadits.edit')->with(['data' => $data]);
    }

    
    public function update(Request $request, $id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $userID      = Auth::id();
        $today       = date("Y-m-d H:i:s"); 

        $updateHadits = [
            'konten'        => $request->konten,
            'update_by'     => $userID,
            'updated_at'    => $today,
        ];

        $updatedata = MHadits::where('id',$id)->update($updateHadits);
        if($updatedata){
            $msg = 'Your data has been successfully updated!';
        return redirect('hadits')->withSuccess($msg);
        } else {
            return redirect()->back()->withInput();
        }
    }

    
    public function destroy($id)
    {
        $deletehadits = MHadits::find($id);
        if($deletehadits) {
            $deletehadits->destroy($id);
            $msg = 'Your data has been deleted!';
        } else {
            $msg = 'Please try again!';
        }
        return redirect()
            ->back()
            ->withSuccess($msg); 
    }
}
