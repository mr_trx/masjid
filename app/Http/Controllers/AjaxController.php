<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class AjaxController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function saveDirectory(Request $request)
    {
        if (isset($_POST['uploading_file'])) {
              $image = $_FILES['post_image']['name'];
            
              // simpan file directory
              $target = public_path('images/') . basename($image);
            //   $fileimage = basename($image);
             
              if (move_uploaded_file($_FILES['post_image']['tmp_name'], $target)) {
                  echo "http://localhost:8282/masjid_v1/" . substr($target,37,500);
                  exit();
              }else{
                  echo "Failed to upload image";
                  exit();
              }
        }
    }
    
}
