@extends('backEND.layouts.index')

@section('page-content')
<div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-header-title">
                            <h4>Dashboard</h4>
                        </div>
                        <div class="page-header-breadcrumb">
                            <ul class="breadcrumb-title">
                                <li class="breadcrumb-item">
                                    <a href="index-2.html">
                                    <i class="icofont icofont-home"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="page-body">
                        <div class="row">
                            <div class="col-md-12 col-xl-6">
                                <p>Selamat Datang dihalaman administrator system Masjid Baitul Ma'mur</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- UNTUK SETTING COLOR -->
                <div id="styleSelector">
                </div>
            </div>
        </div>
    </div>
    
@endsection
