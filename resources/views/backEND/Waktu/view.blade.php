@extends('backEND.layouts.index')

@section('page-content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                {{-- BREADCRUMD --}}
                <div class="page-header">
                    <div class="page-header-title">
                        <h4>Info Waktu Sebelum Azan & Iqomah</h4>
                    </div>
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="index-2.html">
                                <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            {{-- <li class="breadcrumb-item"><a href="#!"></a></li> --}}
                            <li class="breadcrumb-item"><a href="#!">Info Waktu Iqomah</a></li>
                        </ul>
                    </div>
                </div>
                {{-- END BREADCRUMD --}}
                <!-- <div class="page-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Info Waktu Sebelum Azan</h5>
                                    <div class="card-header-right">
                                    @foreach ($waktu_azan['m_waktu_azan'] as $key => $datacount)
                                        @if($datacount->count() != 0)
                                        @else
                                            <a class="btn btn-primary" href="{{ URL('actionviewbeforeazan') }}">
                                                <i class="icofont icofont-ui-add"></i>
                                                Add Waktu Sebelum Azan
                                            </a>
                                        @endif
                                    @endforeach
                                    </div>
                                </div>
                                <div class="card-block">

                                    <table id="example" class="display" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Waktu jeda sebelum azan</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            @foreach ($waktu_azan['m_waktu_azan'] as $key => $dataWaktuAzan)
                                            <tr>
                                                <td>{{ $dataWaktuAzan->waktu }}</td>
                                                
                                                <td>
                                                    <a href="{{ URL('editviewbeforeazan', $dataWaktuAzan->id) }}" type="button" title="Edit" class="btn btn-warning btn-sm">
                                                        <i class="icofont icofont-pencil"></i>
                                                    </a>
                                                    
                                                    <form id="delete" action="{{ URL('actiondeletebeforeazan', $dataWaktuAzan->id) }}" 
                                                        method="GET" style="display: inline;">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                        <button title="Delete" class="btn btn-danger btn-sm"><i class="icofont icofont-trash"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->

                <div class="page-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Info Waktu Iqomah</h5>
                                    <div class="card-header-right">
                                    @foreach ($waktu_iqomah['m_waktu_iqomah'] as $key => $datacountIqomah)
                                        @if($datacountIqomah->count() != 0)
                                            <!-- hanya boleh menambahkan 1 data saja -->
                                        @else
                                        <a class="btn btn-primary" href="{{ URL('actionviewiqomah') }}">
                                            <i class="icofont icofont-ui-add"></i>
                                            Add Waktu Iqomah
                                        </a>
                                        @endif
                                    @endforeach
                                    </div>
                                </div>
                                <div class="card-block">

                                    <table id="example" class="display" style="width:100%">
                                        <thead>
                                            <tr>
                                               <th>Waktu Iqomah</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            @foreach ($waktu_iqomah['m_waktu_iqomah'] as $key => $dataWaktuIqomah)
                                            <tr>
                                               <td>{{ $dataWaktuIqomah->waktu / 60 }}</td>
                                                
                                                <td>
                                                    <a href="{{ url('editviewiqomah', $dataWaktuIqomah->id) }}" type="button" title="Edit" class="btn btn-warning btn-sm">
                                                        <i class="icofont icofont-pencil"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                    

                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!-- UNTUK SETTING COLOR -->
            <div id="styleSelector">
            </div>
        </div>
    </div>
</div>
                
@endsection

@section('js')
<script src="{{ asset('public/asset_admin/assets/js/datatables/js/datatables.min.js') }}"></script>
<!-- <script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script> -->

@endsection