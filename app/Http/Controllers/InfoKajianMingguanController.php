<?php

namespace App\Http\Controllers;

use Hash;
use Auth;
use File;
use Storage;
use View;

use App\MInfoKajianMingguan;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class InfoKajianMingguanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['m_info_kajian_mingguan'] = MInfoKajianMingguan::all();

        return view('backEND.InfoKajianMingguan.view', $data);
    }

    public function create()
    {
        return view('backEND.InfoKajianMingguan.create');
    }

    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $currentuserid  = Auth::id();
        $today          = date("Y-m-d H:i:s"); 

        if($request->photo == "null" || $request->photo == ""){

            $user_photo_user     = 'user_profile.png';
            $path_photo_user     = 'storage/photo_info_kajian_mingguan/user_profile.png'; 

            $all_data_kajian = [
                'nama'          => $request->nama,
                'photo'         => $user_photo_user,
                'photo_path'    => $path_photo_user,
                'tanggal'       => date('Y-m-d', strtotime($request->tanggal)),
                'jam'           => $request->jam,
                'insert_by'     => $currentuserid,
                'created_at'    => $today,
            ];
        } else {

            $uploadedFile   = $request->file('file');        
            $path           = $request->file('photo')->store('public/photo_info_kajian_mingguan');
            $name_photo     = Str::substr(htmlspecialchars($path), 34, 87);
            $substrPath     = Str::substr($path, 6, 100);
            $pathDefault    = "storage".$substrPath;

            $all_data_kajian = [
                'nama'          => $request->nama,
                'photo'         => $name_photo,
                'photo_path'    => $pathDefault,
                'tanggal'       => date('Y-m-d', strtotime($request->tanggal)),
                'jam'           => $request->jam,
                'insert_by'     => $currentuserid,
                'created_at'    => $today,
            ];
        }

        //dd($all_data_kajian);die;
    
        $save = MInfoKajianMingguan::insert($all_data_kajian);
        if($save)
            return redirect('infokajianmingguan');
        else
            return redirect()->back()->withInput();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data['m_info_kajian_mingguan'] = MInfoKajianMingguan::find($id);
     
        return view('backEND.InfoKajianMingguan.edit')->with(['data' => $data]);
    }

    public function update(Request $request, $id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $currentuserid  = Auth::id();
        $today          = date("Y-m-d H:i:s"); 
        $cekData        = MInfoKajianMingguan::where('id', $id)->first(); 
        //dd($request->photo);

        if($request->photo == "null" || $request->photo == ""){
            //dd('masuk 1');
            $user_photo_user     = 'user_profile.png';
            $path_photo_user     = 'storage/photo_info_kajian_mingguan/user_profile.png'; 

            $update_data_kajian = [
                'nama'          => $request->nama,
                'photo'         => $user_photo_user,
                'photo_path'    => $path_photo_user,
                'tanggal'       => date('Y-m-d', strtotime($request->tanggal)),
                'jam'           => $request->jam,
                'update_by'     => $currentuserid,
                'updated_at'    => $today,
            ];

        } else if($cekData['photo'] == $request->photo) {
            //dd('masuk 2');
            $update_data_kajian = [
                'nama'          => $request->nama,
                'tanggal'       => date('Y-m-d', strtotime($request->tanggal)),
                'jam'           => $request->jam,
                'update_by'     => $currentuserid,
                'updated_at'    => $today,
            ];
            
        } else {
            //dd('masuk 3');
            $uploadedFile   = $request->file('file');        
            $path           = $request->file('photo')->store('public/photo_info_kajian_mingguan');
            $name_photo     = Str::substr(htmlspecialchars($path), 34, 87);
            $substrPath     = Str::substr($path, 6, 100);
            $pathDefault    = "storage".$substrPath;

            $update_data_kajian = [
                'nama'          => $request->nama,
                'photo'         => $name_photo,
                'photo_path'    => $pathDefault,
                'tanggal'       => date('Y-m-d', strtotime($request->tanggal)),
                'jam'           => $request->jam,
                'update_by'     => $currentuserid,
                'updated_at'    => $today,
            ];
        }

        //dd($update_data_sholat); die;
        $updatedata = MInfoKajianMingguan::where('id',$id)->update($update_data_kajian);
        if($updatedata){
            $msg = 'Your data has been successfully updated!';
            return redirect('infokajianmingguan')->withSuccess($msg);
        } else {
            return redirect()->back()->withInput();
        }
    }

    public function destroy($id)
    {
        $infoKajian = MInfoKajianMingguan::find($id);
        $cekData    = MInfoKajianMingguan::where('id', $id)->first();
        
        //kondisi hapus file photo
        if($cekData['photo'] == 'user_profile.png'){
            if($infoKajian) {
                $infoKajian->destroy($id);
                $msg = 'Your data has been deleted!';
            } else {
                $msg = 'Please try again!';
            }
            return redirect()
                ->back()
                ->withSuccess($msg); 

        } else {
            $delete_Photo_dir   = $cekData['photo'];

            $filename = public_path().'/storage/photo_info_kajian_mingguan/'.$delete_Photo_dir;
            File::delete($filename);

            if($infoKajian) {
                $infoKajian->destroy($id);
                $msg = 'Your data has been deleted!';
            } else {
                $msg = 'Please try again!';
            }
            return redirect()
                ->back()
                ->withSuccess($msg); 
        }
    }

    public function UpdatePhotoEmptyKajian($id){
        //dd('masuk update Photo');die;
        $cekData          = MInfoKajianMingguan::where('id', $id)->first();
        if($cekData['photo'] == 'user_profile.png'){
            // dd('tidak bisa dihapus');
            // Set kosong pada field photo dan file path
            $emptyPhoto = [
                'photo' => "",
                'photo_path' => "",
            ];

            // update database user per id untuk field photo dan file path
            $updatePhoto = MInfoKajianMingguan::where('id',$id)->update($emptyPhoto);
            if($updatePhoto){
                return redirect()->back()->withInput();
            }
        } else {
            //dd('berhasil dihapus');
            $delete_Photo_dir   = $cekData['photo'];

            $filename = public_path().'/storage/photo_info_kajian_mingguan/'.$delete_Photo_dir;
            File::delete($filename);

            // Set kosong pada field photo dan file path
            $emptyPhoto = [
                'photo' => "",
                'photo_path' => "",
            ];

            // update database user per id untuk field photo dan file path
            $updatePhoto = MInfoKajianMingguan::where('id',$id)->update($emptyPhoto);
            if($updatePhoto){
                return redirect()->back()->withInput();
            }
        }
        
    }
}
