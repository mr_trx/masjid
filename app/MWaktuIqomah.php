<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MWaktuIqomah extends Model
{
    protected $table='m_waktu_iqomah';

    protected $fillable = [
        'roles_user',
        'created_by'
    ];
}
