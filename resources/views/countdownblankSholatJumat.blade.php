<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Masjid Baitul Ma'mur</title>
    <style>
        *{
            margin:0;
            padding:0;
            box-sizing:border-box;
            font-family:"Baloo da 2", cursive;
        }
        html{
            font-size:62.5%;
        }
        .container{
            width:100%;
            height:100vh;
            background-color:#000;
        }
        .countdown-wrapper{
            width:100%;
            position:absolute;
            top:40%;
            text-align:center;
            color:#fff;
        }
        .countdown{
            display:flex;
            justify-content:center;
        }
        .countdown div{
            width:13rem;
            height:13rem;
            background: linear-gradient(
                to bottom,
                rgba(61, 58, 58, 0.9) 50%,
                rgba(99, 93, 93, 0.9) 0
            );
            margin: 0 4rem 12rem 4rem;
            font-size:7rem;
            font-weight:400;
            display:flex;
            justify-content:center;
            align-items:center;
            box-shadow:0 0.8rem 2.5rem rgba(0, 0, 0, 0.5);
            position:relative;
        }
        .countdown div:before{
            content:"";
            position: absolute;
            width:100%;
            height:0.24rem;
            background-color:#17181b;

        }
        .countdown div:after{
            content:attr(data-content);
            font-size: 2.2rem;
            font-weight:400;
            position:absolute;
            bottom:-6rem;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="countdown-wrapper">
            <div class="countdown"></div>
        </div>
    </div>
<script>
    var timer = 600; 
    const countdown = document.querySelector(".countdown");
        
    const interval = setInterval(() => {

            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

    countdown.innerHTML = `
        <div data-content="Menit">${minutes}</div>
        <div>:</div>
        <div data-content="Detik">${seconds}</div>
    `;
    timer--;
    if(timer === 0) {
        SubmitFunction();
        $('#display').empty();
        clearInterval(interVal)
    }
    }, 1000);

    function SubmitFunction(){
		window.location.href = "/masjid_v1";
    }
    
</script>
    
</body>
</html>