<div id="table_sholat" class="row" style="background-color: #c9a5a5;">
                        
    <script type="text/javascript">
        window.setTimeout("waktu()", 1000);
        var date = new Date(); // today
        prayTimes.tune( {subuh: -6, syuruk: -2, dzuhur: 2, ashar: 1, maghrib: 2, isya: 6} );
        var times = prayTimes.getTimes(date, [-6.22742657, 106.95607262, 9], +7);
        var list = ['Subuh', 'Syuruk', 'Dzuhur', 'Ashar', 'Maghrib', 'Isya'];

        var html = '';

        for(var i in list)	{
            html += '<div class="col-md-6"><div class="text-center"><h4 class="title-masjid" style="letter-spacing: 1px;width: 118px;padding: 5px;margin-left: -15px;margin-top: -15px;">'+ list[i]+ '</h4>';
            html += '<h3 style="margin-top: 20px;margin-bottom: 25px;background-color: url({{ URL::asset("public/image/bg_jadwal.png")}});background-repeat: no-repeat;background-size: 100% 100%;">'+ times[list[i].toLowerCase()]+ '</h3>';
            html += '</div></div>';
        }
        
        document.getElementById('table_sholat').innerHTML = html;

        

        // untuk validasi iqomah
        function waktu() {
            var waktu = new Date();
            setTimeout("waktu()", 1000);

            var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum&#39;at', 'Sabtu'];
            var hariJumat = "Jum'at";
            var date = new Date();
            var day = date.getDate();
            var month = date.getMonth();
            var thisDay = date.getDay(),
                thisDay = myDays[thisDay];

            // hapus tanda baca : untuk kebutuhan pengurangan waktu
            var subuh = times['subuh'].substr(1,4);
            var subuhGet = subuh+':00';//console.log(subuhGet);
            var replaceSubuh = subuhGet.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g,"");
           
            var dzuhur = times['dzuhur']+':00';//console.log(dzuhur);
            var replaceDzuhur = dzuhur.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g,"");//console.log(replaceDzuhur);

            var ashar = times['ashar']+':00';
            var replaceAshar = ashar.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g,"");

            var maghrib = times['maghrib']+':00';
            var replaceMagrib = maghrib.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g,"");

            var isya = times['isya']+':00';
            var replaceIsya = isya.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g,"");

            // hapus tanda baca : untuk jam waktu berjalan dan untuk kebutuhan pengurangan
            var jamSekarang = waktu.getHours()+':'+('0'+waktu.getMinutes()).slice(-2)+':'+('0'+waktu.getSeconds()).slice(-2);
            var replaceJam = jamSekarang.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g,"");

           //console.log(jamSekarang);
           //console.log(replaceJam);

            // kondisi pengurangan setiap masing - masing azan
            var untukSubuh  = replaceSubuh - replaceJam;
            var untukDzuhur = replaceDzuhur - replaceJam;
            var UntukAshar  = replaceAshar - replaceJam;
            var untukMagrib = replaceMagrib - replaceJam;
            var untukIsya   = replaceIsya - replaceJam;

            // console.log('jam sekarang : '+jamSekarang);
            //console.log('selisih jam : '+untukDzuhur);
            var sound = new Howl({
                                src: ['public/audio/alarm.mp3'],
                                autoplay: false
                                });     
            
            $.ajax({
                url: "masjid_v1/getWaktuAzan",
                type: "GET",
                cache: false,
                dataType: 'json',
                success: function(dataResult){
                    //console.log(dataResult);
                    var resultData = dataResult.data;
                    var i=1;
                    $.each(resultData,function(index,row){
                        var waktuJedaAzan = row.waktu;                                                                               
                        //console.log('waktu jeda azan :' +waktuJedaAzan);
                        // subuh
                        if(untukSubuh == waktuJedaAzan){//sebelum azan dibuat hitungan detik
                            sound.play();
                            if(untukSubuh == -4){// sesudah waktu azan tiba audio berenti. dibuat waktu jam sekarang dan jam waktu sholat
                                sound.stop();
                            }
                        } else {
                            if(untukSubuh == -8){//kondisi saat susadah azan diberi waktu dalam hitungan menit iqomah dan countdown untuk melaksanakan sholat sunah
                                window.location.href = "prokes";
                            } 
                        }

                        // khusus untuk sholat zuhur & sholat jum'at
                        //console.log(hariJumat);
                        if(thisDay != hariJumat){
                            if(untukDzuhur == waktuJedaAzan){//sebelum azan dibuat hitungan detik
                                sound.play();
                                if(untukDzuhur == -4){
                                    sound.stop();
                                }
                            } else {
                                if(untukDzuhur == -8){
                                    window.location.href = "prokes";
                                } 
                            }
                        } else {
                            if(thisDay == hariJumat){
                                if(untukDzuhur == waktuJedaAzan){//sebelum azan sholat jum'at
                                    sound.play();
                                    if(untukDzuhur == -4){
                                        sound.stop();
                                    }
                                } else {
                                    if(untukDzuhur == -8){
                                        window.location.href = "prokesSholatJumat";
                                    } 
                                }
                            }
                        }

                        // // ashar
                        if(UntukAshar == waktuJedaAzan){
                            sound.play();
                            if(UntukAshar == -4){
                                sound.stop();
                            }
                        } else {
                            if(UntukAshar == -8){
                                window.location.href = "prokes";
                            } 
                        }

                        // // magrib
                        if(untukMagrib == waktuJedaAzan){
                            sound.play();
                            if(untukMagrib == -4){
                                sound.stop();
                            }
                        } else {
                            if(untukMagrib == -8){
                                window.location.href = "prokes";
                            } 
                        }

                        // // isya
                        if(untukIsya == waktuJedaAzan){
                            sound.play();
                            if(untukIsya == -4){
                                sound.stop();
                            }
                        } else {
                            if(untukIsya == -8){
                                window.location.href = "prokes";
                            } 
                        }

                    })
                }
            });

            //console.log('jam : '+ waktu.getHours()+ ' Menit : '+('0'+waktu.getMinutes()).slice(-2)+ ' Detik : '+waktu.getSeconds());
        }
    </script>
    
</div>