<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MInfoSholatJumat extends Model
{
    // protected $table = 'm_info_sholat_jumat';
    protected $table='m_info_sholat_jumat';

    protected $fillable = [
        'roles_user',
        'created_by'
    ];
}
