<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MWaktuAzan extends Model
{
    protected $table='m_waktu_azan';

    protected $fillable = [
        'roles_user',
        'created_by'
    ];
}
