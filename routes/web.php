<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () { return view('welcome');});
Route::get('/', 'FrontendController@index');
Route::get('masjid_v1/getWaktuAzan','FrontendController@getWaktuAzan');
Route::get('prokes', 'ProkesController@index');
Route::get('prokesSholatJumat', 'ProkesController@prokesJumat');
Route::get('masjid_v1/getWaktuIqomah','ProkesController@getWaktuIqomah');
Route::get('countdownblank', 'ProkesController@blankscreen');
Route::get('countdownblankSholatJumat', 'ProkesController@blankscreenJumat');

// * ROUTE BACK END
Route::get('/masjidadminlogin', function () {return view('backEND.loginmasjid');});

Route::group(['middleware' => 'auth'],function(){
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('users', 'UserController');
    Route::resource('infoprogrammasjid', 'InfoProgramMasjidController');
    Route::resource('infosholatjumat', 'InfoSholatJumatController');
    Route::resource('infokajianmingguan', 'InfoKajianMingguanController');
    Route::resource('infokepengurusan', 'InfoKepengurusanController');
    Route::resource('hadits', 'HaditsController');
    Route::resource('jedawaktu', 'JedaWaktuController');
   

    // * view create
    Route::get('actionviewbeforeazan', 'JedaWaktuController@createViewBeforeAzan');
    Route::get('actionviewiqomah', 'JedaWaktuController@createViewIqomah');

    // * action add
    Route::post("actioncreatebeforeazan", "JedaWaktuController@createActionBeforeAzan");
    Route::post("actioncreateiqomah", "JedaWaktuController@createActionIqomah");

    // * view edit
    Route::get('editviewbeforeazan/{id}', 'JedaWaktuController@vieweditBeforeAzan');
    Route::get('editviewiqomah/{id}', 'JedaWaktuController@vieweditIqomah');

    // * action update
    Route::get("actionupdatebeforeazan/{id}", "JedaWaktuController@updateActionBeforeAzan");
    Route::get("actionupdateiqomah/{id}", "JedaWaktuController@updateActionIqomah");

    // * action delete
    // Route::get("actiondeletebeforeazan/{id}", "JedaWaktuController@destroyBeforeAzan");
    // Route::get("actiondeleteiqomah/{id}", "JedaWaktuController@destroyIqomah");
  
    Route::get('UpdatePhotoEmpty/{id}', 'InfoSholatJumatController@UpdatePhotoEmpty')->name('UpdatePhotoEmpty.UpdatePhotoEmpty');
    Route::get('UpdatePhotoEmptyKajian/{id}', 'InfoKajianMingguanController@UpdatePhotoEmptyKajian')->name('UpdatePhotoEmptyKajian.UpdatePhotoEmptyKajian');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
