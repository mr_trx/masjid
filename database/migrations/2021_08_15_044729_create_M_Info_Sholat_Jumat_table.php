<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMInfoSholatJumatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_info_sholat_jumat', function (Blueprint $table) {

            $table->increments('id');
            $table->string('nama', 36);
            $table->string('photo');
            $table->string('photo_path');
            $table->string('sebagai', 36);
            $table->date('tanggal');
            $table->integer('insert_by')->nullable();
            $table->integer('update_by')->nullable();
            $table->timestamps();
 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_info_sholat_jumat');
    }
}
