@extends('backEND.layouts.index')

@section('page-content')

<div class="pcoded-content">
        <div class="pcoded-inner-content">
           <div class="main-body">
              <div class="page-wrapper">
                 {{-- BREADCRUMD --}}
                 <div class="page-header">
                    <div class="page-header-title">
                       <h4>Create Program Masjid</h4>
                    </div>
                    <div class="page-header-breadcrumb">
                       <ul class="breadcrumb-title">
                          <li class="breadcrumb-item">
                             <a href="index-2.html">
                             <i class="icofont icofont-home"></i>
                             </a>
                          </li>
                          <li class="breadcrumb-item"><a href="#!">Program Masjid</a></li>
                          <li class="breadcrumb-item"><a href="#!">Create Program Masjid</a></li>
                       </ul>
                    </div>
                 </div>
                 {{-- END BREADCRUMD --}}
                 <div class="page-body">
                    <div class="row">
                       <div class="col-lg-12">
                          <div class="card">
                             <div class="card-header">
                                <h5>Create Program Masjid</h5>
                             </div>
                             <div class="card-block">
                                 <form id="main" method="post" action="{{ URL('infoprogrammasjid/') }}{{ isset($MInfoProgramMasjids) ? '/' . $MInfoProgramMasjids->id : '' }}" enctype="multipart/form-data" id="myReset">
                                 {{ csrf_field() }}
                                 @if (isset($MInfoProgramMasjids))
                                    {{ method_field('PUT') }}
                                 @endif    
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Pilih Waktu</label>
                                        <div class="col-sm-5">
                                             <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                             <select name="pilih_waktu" class="form-control" id="pilih_waktu">
                                                   <option value="">--- Pilih Waktu ---</option>
                                                   <option value="tanggal">Waktu Tanggal</option>
                                                   <option value="teks">Waktu Teks</option>
                                             </select>
                                        </div>
                                    </div>
                                    <div id="waktu_tanggal" class="form-group row" style="display:none">
                                        <label class="col-sm-2 col-form-label">Waktu Tanggal</label>
                                        <div class="col-sm-5">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input 
                                            id="dropper-default" 
                                            class="form-control form-control-danger" 
                                            type="text" 
                                            name="tanggal" 
                                            >
                                        </div>
                                    </div>
                                    <div id="waktu_teks" class="form-group row" style="display:none">
                                        <label class="col-sm-2 col-form-label">Waktu Teks</label>
                                        <div class="col-sm-5">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input 
                                            class="form-control form-control-danger" 
                                            type="text" 
                                            name="waktu_teks" 
                                            >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Judul Kegiatan</label>
                                        <div class="col-sm-10">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="text" name="judul" class="form-control form-control-danger">
                                            <span class="messages"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Acara Kegiatan</label>
                                        <div class="col-sm-10">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="file" id="image-input" style="display: none;">
                                            <textarea rows="5" cols="5" class="form-control form-control-danger" name="konten"></textarea>
                                        </div>
                                    </div>
                                   
                                    <div class="form-group row">
                                        <label class="col-sm-2"></label>
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-danger m-b-0">Reset</button>
                                            <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                        </div>
                                    </div>
                                </form>
            
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </div>
              <!-- UNTUK SETTING COLOR -->
              <div id="styleSelector"></div>
           </div>
        </div>
     </div>

@endsection

@section('js')
<script src="{{ asset('public/vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('public/vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/asset_admin/bower_components/datedropper/datedropper.min.js') }}"></script>
<script>
$("#pilih_waktu").change(function() {
   if ($(this).val() == "tanggal") {
         $("#waktu_tanggal").show();
   } else  {
         $("#waktu_tanggal").hide();
   }

   if ($(this).val() == "teks") {
         $("#waktu_teks").show();
   } else  {
         $("#waktu_teks").hide();
   }
});
</script>

<script>
   $('#dropper-default').dateDropper({
      theme: 'leaf'
   });
</script> 
<script>
    $('textarea').ckeditor();
    // $('.textarea').ckeditor(); // if class is prefered.
</script>
@endsection