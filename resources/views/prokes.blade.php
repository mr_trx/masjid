<!DOCTYPE html>
<html>
<head>
	<title>Masjid Baitul Ma'mur</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/prokes/css/custom.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/prokes/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/prokes/css/grid.css') }}">
</head>
<body>
<div class="container" style="width: 1435px;padding-top: 50px;">
    <div class="row row-cols-2">
        <div class="col-10">
            <img src="{{URL::asset('public/image/prokes2.jpg')}}" style="margin-bottom: 15px;">
        </div>
        <div class="col-2" >
            <div> 
                <h3 style="color: #fff;text-align: center;margin-top: 210px;margin-bottom:-230px;">Waktu Sisa Iqomah</h3>
                <h1 id="display" style="font-size: 5vh;color: #fff;text-align: center;margin-top: 250px;"></h1>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('public/js/jquery-3.3.1.min.js') }}"></script> 
<script type="text/javascript" src="{{ asset('public/js/howler.core.js') }}"></script>
<script type="text/javascript">
    $.ajax({
        url: "masjid_v1/getWaktuIqomah",
        type: "GET",
        cache: false,
        dataType: 'json',
        success: function(dataResult){
            //console.log(dataResult);
            var resultData = dataResult.data;
            var i=1;
            $.each(resultData,function(index,row){
                window.waktuIqomah = row.waktu; 
                //var timealert = 10;
                CountDown(waktuIqomah);
            })
        }
    });

    function CountDown() {
        var timer = waktuIqomah; 
        var interVal=  setInterval(function () {
            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            $(display).html("<b>" + minutes + "m : " + seconds + "s" + "</b>");
            var sound = new Howl({
                                src: ['public/audio/alarm.mp3'],
                                preload:true,
                                autoplay:false,
                                loop:true
                                });  
            timer--;
            if(timer == 20) {
                sound.play();
            } else {
                if(timer === 0) {
                    sound.stop();
                    SubmitFunction();
                    $('#display').empty();
                    clearInterval(interVal)
                }
            }
        },1000);
        
    }
        
    function SubmitFunction(){
		window.location.href = "countdownblank";
    }
    
    //CountDown(10,$('#display'));  
</script>
</body>
</html>