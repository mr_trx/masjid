<div class="col-md-3" style="margin-top: -268px;">
    <div class="text-center" style="padding: 5px;margin: 0 auto;width: 100%;">
        <h3 style="letter-spacing: 1px;line-height: 1px;margin-top: 0;">
            <b style="color:#d46327;">( </b>Info Sholat Jum'at Bulan ini
        </h3>
    </div>
    <!-- dibuat tampilan slideshow -->
    
    <div class="ct-box-slider ct-js-box-slider">
    @foreach($dataSholat->chunk(4) as $three)
        <div class="item">
            <div class="container-fluid" style="margin-top:20px">
                <div class="row" style="max-height: 390px;">
                    @foreach ($three as $dataAllSholatJumat)
                        @if($dataAllSholatJumat->count() != 0)
                            <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12" style="padding-top:0 !important;">
                                <div onclick="location.href='#'" class="ct-box">
                                    <img src="{{ asset('public/storage/photo_info_sholat_jumat/'.$dataAllSholatJumat->photo) }}" class="img-thumbnail" style="margin: 0 auto; max-width:100px; margin-top: -7px;">
                                    <div class="inner" role="presentation">
                                        <div class="title">
                                            <h5 style="font-family:'Calibri';margin-top: 3px;font-size: 17px;">{{ $dataAllSholatJumat->nama }}</h5>
                                        </div>
                                        <div class="title">
                                            <h5 style="font-family:'Calibri';font-size: 17px;">{{ $dataAllSholatJumat->sebagai }}</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <p>Maaf data info sholat jum'at bulan ini tidak ada</p>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
        @endforeach
    </div>
    
    <!-- <div class="text-right ct-box-slider__arrows clearfix">
        <button id="ct-js-box-slider--next" type="button" class="pull-right slick-arrow">
            <img alt="Previous" src="{{URL::asset('public/image/arrow-next.png')}}" alt="Arrow Next" style="width: 10px;">
        </button>
        <button id="ct-js-box-slider--prev" type="button" class="pull-right slick-arrow">
            <img alt="Next" src="{{URL::asset('public/image/arrow-prev.png')}}" alt="Arrow Prev" style="width: 10px;">
        </button>
    </div> -->
</div>