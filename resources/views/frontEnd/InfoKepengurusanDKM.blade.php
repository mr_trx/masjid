<div class="text-center" style="padding: 5px;margin: 0 auto;width: 100%;">
    <h3 style="letter-spacing: 1px;margin-top: 0px;">
        <b style="color:#d46327;">( </b>Info Kepengurusan DKM
    </h3>
</div>

<div class="container">
    <div class="col">
        <div class="meta mb-1">
            <table width="375" style="font-family: calibri;margin-top:-10px;">
                <tbody style="line-height:25px;">
                    <tr>
                        <td style="letter-spacing:1px;font-size: 18px;">Ketua</td>
                        <td style="letter-spacing:1px;font-size: 18px;">:</td>
                        <td style="letter-spacing:1px;font-size: 18px;">Budianto</td>
                    </tr>
                    <tr>
                        <td style="letter-spacing:1px;font-size: 18px;">Sekretaris</td>
                        <td style="letter-spacing:1px;font-size: 18px;">:</td>
                        <td style="letter-spacing:1px;font-size: 18px;">Agung Wahono & Eddy Yulianto</td>
                        <!-- <tr>
                            <td style="letter-spacing:1px;font-size: 18px;"></td>
                            <td style="letter-spacing:1px;font-size: 18px;"></td>
                            <td style="letter-spacing:1px;font-size: 18px;">Eddy Yulianto</td>
                        </tr> -->
                    </tr>
                    <tr>
                        <td style="letter-spacing:1px;font-size: 18px;">Bendahara</td>
                        <td style="letter-spacing:1px;font-size: 18px;">:</td>
                        <td style="letter-spacing:1px;font-size: 18px;">Sulistyo & Okta Wirawan</td>
                        <!-- <tr>
                            <td style="letter-spacing:1px;font-size: 18px;"></td>
                            <td style="letter-spacing:1px;font-size: 18px;"></td>
                            <td style="letter-spacing:1px;font-size: 18px;">Okta Wirawan </td>
                        </tr> -->
                        
                    </tr>
                    <tr>
                        <td style="letter-spacing:1px;font-size: 18px;">Alamat
                            <!-- <picture>
                                <img class="img-thumbnail" src="public/image/giphy.webp" style="margin: 0 auto; max-width:100px;">
                            </picture> -->
                        </td>
                        <td style="letter-spacing:1px;font-size: 18px;">:</td>
                        <td style="letter-spacing:1px;font-size: 18px;padding:2px;">Sekretariat Masjid Baitul Ma’mur
                            Grand Prima Bintara Blok D5 No.1-2 
                            RT02 RW016</td>
                    </tr>
                </tbody>
            </table>
        </div>
        
        <!-- <ol>
            <li>Developer A (081244224442)</li>
            <li>Developer A (081244224442)</li>
            <li>Developer A (081244224442)</li>
        </ol> -->
    </div>
</div>