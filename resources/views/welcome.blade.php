<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>MASJID BAITUL MA'MUR</title>
        <!-- <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css"> -->
        <link rel="stylesheet" type="text/css" href="{{ asset('public/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('public/css/grid.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('public/css/style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('public/css/all.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('public/css/jquerysctipttop.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('public/css/slick.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('public/css/slick.min.css.map') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('public/css/custom.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('public/css/action-grid-slider.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('public/css/slider.css') }}">

    </head>

    <body>
    <script type="text/javascript" src="{{ asset('public/js/jquery-3.3.1.min.js') }}"></script> 
    <script type="text/javascript" src="{{ asset('public/js/PrayTimes.js') }}"></script>  
    <script type="text/javascript" src="{{ asset('public/js/slider.js') }}"></script>

    <div class="container-fluid halaman_utama">
        <div class="row">
            <div class="col-md-2">

                <!-- start content logo,tanggal, dan jam -->
                @include('frontEnd.Logo')
                <!-- end content logo,tanggal, dan jam -->

                <!-- start content jam sholat -->
                @include('frontEnd.JadwalSholat')
                <!-- end content jam sholat -->

            </div>

            <!-- start content Info Program Masjid Baitul Ma'mur -->
            @include('frontEnd.InfoProgramMasjid')
            <!-- end content Info Program Masjid Baitul Ma'mur -->
            
            <!-- start content slideshow hadits-hadits -->
            @include('frontEnd.Hadits')
            <!-- end content slideshow hadits-hadits -->
        
        </div>
       

        <div class="row">
            <!-- start content info cuaca -->
            @include('frontEnd.InfoCuaca')
            <!-- end content info cuaca -->

            <!-- start content Info Sholat Jum'at Bulan ini -->
            @include('frontEnd.InfoSholatJumat')
            <!-- end content Info Sholat Jum'at Bulan ini -->

            <!-- start content Info Kajian Mingguan -->
            @include('frontEnd.InfoKajian')
            <!-- end content Info Kajian Mingguan -->

            <div class="col-md-4" style="margin-top: -268px;">
                <!-- start content Berita-berita Islami -->
                @include('frontEnd.BeritaPlaylist')
                <!-- end content Berita-berita Islami -->

                <!-- start content Info Kepengurusan DKM -->
                @include('frontEnd.InfoKepengurusanDKM')
                <!-- end content Info Kepengurusan DKM -->
            </div>
        </div>

    </div>
  
    <script type="text/javascript" src="{{ asset('public/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/howler.core.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/slick.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/action-grid-slider.js') }}"></script>
    <script type="text/javascript">
        var interval;
        function ticker()
        {
            if($(".rightSideAreaNews .block").length > 3)
            {
                $(".rightSideAreaNews .block:first").slideUp(function(){
                    $(this).appendTo(".rightSideAreaNews").slideDown();
                });
            }

            if($(".rightSideAreaNews1 .block1").length > 3)
            {
                $(".rightSideAreaNews1 .block1:first").slideUp(function(){
                    $(this).appendTo(".rightSideAreaNews1").slideDown();
                });
            }
            function stop()
            {
                clearInterval(interval, 7000);
            }
        }

        $(document).ready(function(){
                interval = setInterval(ticker,7000);
            $(".rightSideAreaNews").hover(function(){
                stop();
            },function(){
                interval = setInterval(ticker,7000);
            });
        });
        $(document).ready(function(){
                interval = setInterval(ticker,7000);
            $(".rightSideAreaNews1").hover(function(){
                stop();
            },function(){
                interval = setInterval(ticker,7000);
            });
        });
    </script>
</body>
</html>
