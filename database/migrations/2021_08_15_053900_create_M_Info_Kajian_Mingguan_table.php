<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMInfoKajianMingguanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_info_kajian_mingguan', function (Blueprint $table) {

            $table->increments('id');
            $table->string('nama', 36);
            $table->string('photo');
            $table->string('photo_path');
            $table->date('tanggal');
            $table->string('jam');
            $table->integer('insert_by')->nullable();
            $table->integer('update_by')->nullable();
            $table->timestamps();
 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_info_kajian_mingguan');
    }
}
