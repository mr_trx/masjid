<nav class="pcoded-navbar" pcoded-header-position="relative">
    <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
    <div class="pcoded-inner-navbar main-menu">

        <div class="pcoded-navigatio-lavel" data-i18n="nav.category.navigation" menu-title-theme="theme5">Navigation</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="active pcoded-trigger">
                <a href="{{ url('home') }}">
                <span class="pcoded-micon"><i class="ti-home"></i></span>
                <span class="pcoded-mtext">Dashboard</span>
                <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="">
                <a href="{{ URL('users') }}">
                <span class="pcoded-micon"><i class="ti-user"></i></span>
                <span class="pcoded-mtext">Data User</span>
                <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>

        {{-- <div class="pcoded-navigatio-lavel" data-i18n="nav.category.navigation" menu-title-theme="theme5">Blogger</div> --}}
        <ul class="pcoded-item pcoded-left-item">
            <li class=" ">
                <a href="{{ URL('infoprogrammasjid') }}" data-i18n="nav.widget.main">
                <span class="pcoded-micon"><i class="ti-view-grid"></i></span>
                <span class="pcoded-mtext">Info Program Masjid</span>
                <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class=" ">
                <a href="{{ URL('infosholatjumat') }}" data-i18n="nav.widget.main">
                <span class="pcoded-micon"><i class="ti-comment-alt"></i></span>
                <span class="pcoded-mtext">Info Sholat Jum'at</span>
                <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class=" ">
                <a href="{{ URL('infokajianmingguan') }}" data-i18n="nav.widget.main">
                <span class="pcoded-micon"><i class="ti-comment-alt"></i></span>
                <span class="pcoded-mtext">Info Kajian Mingguan</span>
                <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>

        {{-- <div class="pcoded-navigatio-lavel" data-i18n="nav.category.navigation" menu-title-theme="theme5">Profile</div> --}}
        <ul class="pcoded-item pcoded-left-item">
            <!-- <li class=" ">
                <a href="{{ URL('infokepengurusan') }}" data-i18n="nav.widget.main">
                <span class="pcoded-micon"><i class="ti-blackboard"></i></span>
                <span class="pcoded-mtext">Info Kepengurusan DKM</span>
                <span class="pcoded-mcaret"></span>
                </a>
            </li> -->
            <li class=" ">
                <a href="{{ URL('hadits') }}" data-i18n="nav.widget.main">
                <span class="pcoded-micon"><i class="ti-book"></i></span>
                <span class="pcoded-mtext">Hadits-hadits</span>
                <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class=" " style="height: 55px;">
                <a href="{{ URL('jedawaktu') }}" data-i18n="nav.widget.main">
                <span class="pcoded-micon"><i class="ti-book"></i></span>
                <span class="pcoded-mtext">Atur Waktu Iqomah</span>
                <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
        


    </div>
</nav>
