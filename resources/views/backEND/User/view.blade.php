@extends('backEND.layouts.index')

@section('page-content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                {{-- BREADCRUMD --}}
                <div class="page-header">
                    <div class="page-header-title">
                        <h4>User</h4>
                    </div>
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="index-2.html">
                                <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            {{-- <li class="breadcrumb-item"><a href="#!"></a></li> --}}
                            <li class="breadcrumb-item"><a href="#!">User</a></li>
                        </ul>
                    </div>
                </div>
                {{-- END BREADCRUMD --}}
                <div class="page-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Data User</h5>
                                    <div class="card-header-right">
                                        <a class="btn btn-primary" href="{{ URL('users/create') }}">
                                            <i class="icofont icofont-ui-add"></i>
                                            Add Account
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block">

                                    <table id="example" class="display" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone/Mobile</th>
                                                <th>Role</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            @foreach ($users as $key => $data)
                                            <tr>
                                                <td>{{ ($key+1) }}</td>
                                                <td>{{ $data->name }}</td>
                                                <td>{{ $data->email }}</td>
                                                <td>{{ $data->phone }}</td>
                                                @if ($data->role == 1)
                                                    <td>Administrator</td>
                                                @endif
                                                
                                                <td>
                                                    <a href="{{ route('users.edit', $data->id) }}" type="button" title="Edit" class="btn btn-warning btn-sm">
                                                        <i class="icofont icofont-pencil"></i>
                                                    </a>
                                                    
                                                    <a href="{{ route('users.destroy', $data->id) }}" type="button" title="Delete" class="btn btn-danger btn-sm" 
                                                            onclick="event.preventDefault(); document.getElementById('delete').submit();">
                                                        <i class="icofont icofont-trash"></i>
                                                    </a>
                                                    <form id="delete" action="{{ route('users.destroy', $data->id) }}" 
                                                        method="POST" style="display: none;">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                    </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                    

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- UNTUK SETTING COLOR -->
            <div id="styleSelector">
            </div>
        </div>
    </div>
</div>
                
@endsection

@section('js')
<script src="{{ asset('public/asset_admin/assets/js/datatables/js/datatables.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>

@endsection