<?php

namespace App\Http\Controllers;

use Hash;
use Auth;
use File;
use Storage;
use View;

use App\User;
use App\MWaktuAzan;
use App\MWaktuIqomah;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class JedaWaktuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $waktu_azan['m_waktu_azan'] = MWaktuAzan::all();
        $waktu_iqomah['m_waktu_iqomah'] = MWaktuIqomah::all();

        return view('backEND.Waktu.view')->with(['waktu_azan' => $waktu_azan, 'waktu_iqomah' => $waktu_iqomah]);
    }

    public function createViewBeforeAzan()
    {
        return view('backEND.Waktu.createBeforeAzan');
    }

    public function createViewIqomah()
    {
        return view('backEND.Waktu.createIqomah');
    }

    
    public function createActionBeforeAzan(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $userID      = Auth::id();
        $today       = date("Y-m-d H:i:s"); 

        $data = [
            'waktu'        => $request->waktu,
            'insert_by'     => $userID,
            'created_at'    =>  $today,
        ];
    
        $save = MWaktuAzan::insert($data);
        if($save)
            return redirect('jedawaktu');
        else
            return redirect()->back()->withInput();
    }

    public function createActionIqomah(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $userID      = Auth::id();
        $today       = date("Y-m-d H:i:s"); 

        $data = [
            'waktu'        => $request->waktu * 60,
            'insert_by'     => $userID,
            'created_at'    =>  $today,
        ];

        //dd($data);die;
        $save = MWaktuIqomah::insert($data);
        if($save)
            return redirect('jedawaktu');
        else
            return redirect()->back()->withInput();
    }



    public function vieweditBeforeAzan($id)
    {
        $data['m_waktu_azan'] = MWaktuAzan::find($id);
     
        return view('backEND.Waktu.editBeforeAzan')->with(['data' => $data]);
    }

    public function vieweditIqomah($id)
    {
        $data['m_waktu_iqomah'] = MWaktuIqomah::find($id);
     
        return view('backEND.Waktu.editIqomah')->with(['data' => $data]);
    }

    
    public function updateActionBeforeAzan(Request $request, $id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $userID      = Auth::id();
        $today       = date("Y-m-d H:i:s"); 

        $dataupdate = [
            'waktu'        => $request->waktu,
            'update_by'     => $userID,
            'updated_at'    => $today,
        ];

        $updatedata = MWaktuAzan::where('id',$id)->update($dataupdate);
        if($updatedata){
            $msg = 'Your data has been successfully updated!';
            return redirect('jedawaktu')->withSuccess($msg);
        } else {
            return redirect()->back()->withInput();
        }
    }

    public function updateActionIqomah(Request $request, $id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $userID      = Auth::id();
        $today       = date("Y-m-d H:i:s"); 

        $dataupdateIqomah = [
            'waktu'        => $request->waktu * 60,
            'update_by'     => $userID,
            'updated_at'    =>  $today,
        ];

        //dd($dataupdateIqomah);die;
        $updatedata = MWaktuIqomah::where('id',$id)->update($dataupdateIqomah);
        if($updatedata)
            return redirect('jedawaktu');
        else
            return redirect()->back()->withInput();
    }

    
    // public function destroyBeforeAzan($id)
    // {
    //     dd('destroyBeforeAzan');
    //     $deleteData = MWaktuAzan::find($id);
    //     if($deleteData) {
    //         $deleteData->destroy($id);
    //         $msg = 'Your data has been deleted!';
    //     } else {
    //         $msg = 'Please try again!';
    //     }
    //     return redirect()
    //         ->back()
    //         ->withSuccess($msg); 
    // }

    // public function destroyIqomah($id)
    // {
    //     //
    // }
}
