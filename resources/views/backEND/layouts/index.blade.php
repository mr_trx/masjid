<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
   <head>
      <title>Admin - Masjid Baitul Ma'mur</title>
      <meta name="csrf-token" content="{{ csrf_token() }}">
      
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
      <meta name="description" content="Phoenixcoded">
      <meta name="keywords" content=", Flat ui, Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
      <meta name="author" content="Phoenixcoded">
      <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="{{ asset('public/asset_admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('public/asset_admin/assets/icon/themify-icons/themify-icons.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('public/asset_admin/assets/icon/icofont/css/icofont.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('public/asset_admin/assets/pages/flag-icon/flag-icon.min.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('public/asset_admin/assets/pages/menu-search/css/component.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('public/asset_admin/assets/pages/dashboard/horizontal-timeline/css/style.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('public/asset_admin/assets/pages/dashboard/amchart/css/amchart.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('public/asset_admin/assets/pages/flag-icon/flag-icon.min.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('public/asset_admin/assets/css/style.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('public/asset_admin/assets/css/strukturrt15.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('public/asset_admin/assets/css/color/color-1.css') }}" id="color" />
      <link rel="stylesheet" type="text/css" href="{{ asset('public/asset_admin/assets/css/linearicons.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('public/asset_admin/assets/css/simple-line-icons.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('public/asset_admin/assets/css/ionicons.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('public/asset_admin/assets/css/jquery.mCustomScrollbar.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('public/asset_admin/assets/css/datatables/css/datatables.min.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('public/asset_admin/assets/pages/advance-elements/css/bootstrap-datetimepicker.css') }}">
      
      <link rel="stylesheet" type="text/css" href="{{ asset('public/asset_admin/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" />
      <link rel="stylesheet" type="text/css" href="{{ asset('public/asset_admin/bower_components/datedropper/datedropper.min.css') }}" />
   </head>
   <body>
      <div class="theme-loader">
         <div class="ball-scale">
            <div></div>
         </div>
      </div>
      <div id="pcoded" class="pcoded">
         <div class="pcoded-overlay-box"></div>
         <div class="pcoded-container navbar-wrapper">
            <nav class="navbar header-navbar pcoded-header" header-theme="theme4">
               <div class="navbar-wrapper">
                  <div class="navbar-logo">
                     <a class="mobile-menu" id="mobile-collapse" href="#!">
                     <i class="ti-menu"></i>
                     </a>
                     <a href="#" style="margin-top: -15px;">Masjid Baitul Ma'mur
                     <!-- <img class="img-fluid" src="{{ asset('public/asset_admin/assets/images/um_logo.png') }}" alt="Theme-Logo" /> -->
                     </a>
                     <a class="mobile-options">
                     <i class="ti-more"></i>
                     </a>
                  </div>
                  <div class="navbar-container container-fluid">
                     <div>
                        <ul class="nav-left">
                           <li>
                              <a href="#!" onclick="javascript:toggleFullScreen()">
                              <i class="ti-fullscreen"></i>
                              </a>
                           </li>
                        </ul>
                        <ul class="nav-right">
                          
                           <li class="header-notification">
                              <a id="jam" href="#!" class="" style="cursor:auto;">
                                 
                              </a>
                           </li>
                           <script type="text/javascript">
                              window.onload = function() { jam(); }
                              function jam() {
                                 var e = document.getElementById('jam'),
                                 d = new Date(), h, m, s;
                                 h = d.getHours();
                                 m = set(d.getMinutes());
                                 s = set(d.getSeconds());
                                 e.innerHTML = h +':'+ m +':'+ s;
                                 setTimeout('jam()', 1000);
                              }
                              function set(e) {
                                 e = e < 10 ? '0'+ e : e;
                                 return e;
                              }
                           </script>
                           
                           {{-- USER  --}}
                           <li class="user-profile header-notification">
                              <a href="#!">
                              <img src="{{ asset('public/asset_admin/assets/images/user.png') }}" alt="User-Profile-Image">
                              @guest
                              {{ URL('/') }}
                              @else
                                    <span> {{ Auth::user()->name }}</span>
                              @endguest
                              {{-- <span>John Doe</span> --}}
                              <i class="ti-angle-down"></i>
                              </a>
                              <ul class="show-notification profile-notification">
                                
                                 <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                       <i class="ti-layout-sidebar-left"></i> Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                       {{ csrf_field() }}
                                    </form>
                                 </li>
                              </ul>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </nav>
          
            <div class="pcoded-main-container">
               <div class="pcoded-wrapper">

                  @include('backEND.layouts.menus')

                  
                  @yield('page-content')

               </div>
            </div>

         </div>
      </div>
     
      <script type="text/javascript" src="{{ asset('public/asset_admin/bower_components/jquery/dist/jquery.min.js') }}"></script>
      <script src="{{ asset('public/asset_admin/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('public/asset_admin/bower_components/tether/dist/js/tether.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('public/asset_admin/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('public/asset_admin/bower_components/jquery-slimscroll/jquery.slimscroll.js') }}"></script>
      <script type="text/javascript" src="{{ asset('public/asset_admin/bower_components/modernizr/modernizr.js') }}"></script>
      <script type="text/javascript" src="{{ asset('public/asset_admin/bower_components/modernizr/feature-detects/css-scrollbars.js') }}"></script>
      <script type="text/javascript" src="{{ asset('public/asset_admin/bower_components/classie/classie.js') }}"></script>
      <script src="{{ asset('public/asset_admin/bower_components/d3/d3.js') }}"></script>
      <script src="{{ asset('public/asset_admin/bower_components/rickshaw/rickshaw.js') }}"></script>
      <script src="{{ asset('public/asset_admin/bower_components/raphael/raphael.min.js') }}"></script>
      <script src="{{ asset('public/asset_admin/bower_components/morris.js/morris.js') }}"></script>
      
      <script type="text/javascript" src="{{ asset('public/asset_admin/assets/pages/dashboard/horizontal-timeline/js/main.js') }}"></script>
      <script type="text/javascript" src="{{ asset('public/asset_admin/assets/pages/dashboard/amchart/js/amcharts.js') }}"></script>
      <script type="text/javascript" src="{{ asset('public/asset_admin/assets/pages/dashboard/amchart/js/serial.js') }}"></script>
      <script type="text/javascript" src="{{ asset('public/asset_admin/assets/pages/dashboard/amchart/js/light.js') }}"></script>
      {{-- <script type="text/javascript" src="{{ asset('public/asset_admin/assets/pages/dashboard/amchart/js/custom-amchart.js') }}"></script> --}}
      <script type="text/javascript" src="{{ asset('public/asset_admin/bower_components/i18next/i18next.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('public/asset_admin/bower_components/i18next-xhr-backend/i18nextXHRBackend.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('public/asset_admin/bower_components/i18next-browser-languagedetector/i18nextBrowserLanguageDetector.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('public/asset_admin/bower_components/jquery-i18next/jquery-i18next.min.js') }}"></script>
      {{-- <script type="text/javascript" src="{{ asset('public/asset_admin/assets/pages/dashboard/custom-dashboard.js') }}"></script> --}}
      <script type="text/javascript" src="{{ asset('public/asset_admin/assets/js/script.js') }}"></script>
      <script src="{{ asset('public/asset_admin/assets/js/pcoded.min.js') }}"></script>
      <script src="{{ asset('public/asset_admin/assets/js/demo-12.js') }}"></script>
      <script src="{{ asset('public/asset_admin/assets/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
      <script src="{{ asset('public/asset_admin/assets/js/jquery.mousewheel.min.js') }}"></script>

      @yield('js');
   </body>
   
</html>