@extends('backEND.layouts.index')

@section('page-content')

<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                {{-- BREADCRUMD --}}
                <div class="page-header">
                <div class="page-header-title">
                    <h4>Create Waktu Sebelum Azan</h4>
                </div>
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index-2.html">
                            <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Info Waktu Sebelum Azan & Iqomah</a></li>
                        <li class="breadcrumb-item"><a href="#!">Create Waktu Sebelum Azan</a></li>
                    </ul>
                </div>
                </div>
                {{-- END BREADCRUMD --}}
                <div class="page-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                            <h5>Create Waktu Sebelum Azan</h5>
                            </div>
                            <div class="card-block">
                            <form id="main" method="post" action="{{ url('actioncreatebeforeazan')  }}" enctype="multipart/form-data" id="myReset">
                            {{ csrf_field() }}
                            @if (isset($MWaktuAzan))
                                {{ method_field('PUT') }}
                            @endif    
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Jeda waktu sebelum Azan</label>
                                    <div class="col-sm-5">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input 
                                        class="form-control form-control-danger" 
                                        name="waktu" 
                                        type="number" max="60"
                                        required
                                        >
                                        <span class="messages">*  Dibuat dalam hitungan detik sebulam azan tiba (maximal 60)</span><br/>
                                        <span class="messages">*  contoh: jam azan 12:00:00 format(12 = jam : 00 = menit : 00 = detik). Jika form diisi 50 berarti system akan berjalan di waktu jam sebelum azan tiba waktu 12:59:50</span>
                                    </div>
                                </div>                            
                                <div class="form-group row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-danger m-b-0">Reset</button>
                                        <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                    </div>
                                </div>
                            </form>
        
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <!-- UNTUK SETTING COLOR -->
            <div id="styleSelector"></div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script src="{{ asset('public/vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
<script>
  function maxLengthCheck(object)
  {
    if (object.value.length > object.maxLength)
      object.value = object.value.slice(0, object.maxLength)
  }
</script>

@endsection