@extends('backEND.layouts.index')

@section('page-content')

<div class="pcoded-content">
   <div class="pcoded-inner-content">
      <div class="main-body">
         <div class="page-wrapper">
            {{-- BREADCRUMD --}}
            <div class="page-header">
               <div class="page-header-title">
                  <h4>Create User</h4>
               </div>
               <div class="page-header-breadcrumb">
                  <ul class="breadcrumb-title">
                     <li class="breadcrumb-item">
                        <a href="index-2.html">
                        <i class="icofont icofont-home"></i>
                        </a>
                     </li>
                     <li class="breadcrumb-item"><a href="#!">User</a></li>
                     <li class="breadcrumb-item"><a href="#!">Create User</a></li>
                  </ul>
               </div>
            </div>
            {{-- END BREADCRUMD --}}
            <div class="page-body">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="card">
                        <div class="card-header">
                           <h5>Create User</h5>
                           <br/>
                           <!-- <p>dssfddfssd</p> -->
                        </div>
                        
                        <div class="card-block">
                        <form method="post" action="{{ URL('users/') }}{{ isset($user) ? '/' . $user->id : '' }}" enctype="multipart/form-data" id="myReset">
                           {{ csrf_field() }}
                              <div class="form-group row">
                                 <label class="col-sm-2 col-form-label">Nama Lengkap</label>
                                 <div class="col-sm-10">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input 
                                       type="text" 
                                       class="form-control form-control-danger" 
                                       name="name" 
                                       placeholder="Nama Lengkap" 
                                       required
                                    >
                                    @if (session()->has('name'))
                                       <span class="messages">*  {{ session('name') }}</span>
                                    @endif
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label class="col-sm-2 col-form-label">Email</label>
                                 <div class="col-sm-10">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input 
                                       type="email" 
                                       name="email"
                                       class="form-control form-control-danger" 
                                       id="email" 
                                       placeholder="contoh : admin@example.com" 
                                       required
                                    >
                                    @if (session()->has('email'))
                                       <span class="messages">*  {{ session('email') }}</span>
                                    @endif
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label class="col-sm-2 col-form-label">Tempat Tanggal Lahir</label>
                                 <div class="col-sm-5">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input 
                                       type="text" 
                                       class="form-control form-control-danger" 
                                       name="tempat_lahir" 
                                       placeholder="Tempat Lahir" 
                                       required
                                    >
                                 </div>
                                 <div class="col-sm-5">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input 
                                       id="dropper-default" 
                                       class="form-control form-control-danger" 
                                       type="text" 
                                       name="tanggal_lahir" 
                                       placeholder="Tanggal Lahir" 
                                       required
                                    >
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label class="col-sm-2 col-form-label">No Handphone</label>
                                 <div class="col-sm-10">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input 
                                       type="number" 
                                       class="form-control form-control-danger" 
                                       name="phone" 
                                       placeholder="No Handphone" 
                                       pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==13) return false;" 
                                       required
                                    >
                                    <span class="messages"></span>
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label class="col-sm-2 col-form-label">Upload Photo</label>
                                 <div class="col-sm-10">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input 
                                       type="file" 
                                       name="photo" 
                                       class="form-control form-control-danger"
                                    >

                                    @if (session()->has('photo'))
                                       <span class="messages">* {{ session('photo') }}</span>
                                    @else
                                       <span class="messages" style="color:red">Format File Photo : JPEG, JPG, PNG</span>
                                    @endif
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label class="col-sm-2 col-form-label">Password</label>
                                 <div class="col-sm-10">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input 
                                       type="password" 
                                       class="form-control form-control-danger" 
                                       id="password" 
                                       name="password" 
                                       placeholder="Password" 
                                       required
                                    >

                                    @if (session()->has('password'))
                                       <span class="messages">* {{ session('password') }}</span>
                                    @endif
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label class="col-sm-2 col-form-label">Repeat Password</label>
                                 <div class="col-sm-10">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input 
                                       type="password" 
                                       class="form-control form-control-danger" 
                                       id="repeat-password" 
                                       name="password_confirmation" 
                                       placeholder="Ulangi password diatas" 
                                       required
                                    >

                                    @if (session()->has('password_confirmation'))
                                       <span class="messages">* {{ session('password_confirmation') }}</span>
                                    @endif
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label class="col-sm-2 col-form-label">Role User</label>
                                 <div class="col-sm-10">
                                    @foreach($getRole as $index => $dataRole)
                                    <div class="form-check form-check-horizontal radio-danger">
                                          <label class="form-check-label">
                                             <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                             <input class="form-check-input" type="radio" name="role" value="{{$dataRole->id}}"> {{$dataRole->roles_user}}
                                          </label>
                                    </div>
                                    @endforeach

                                    @if (session()->has('role'))
                                       <span class="messages"> | * {{ session('role') }}</span>
                                    @endif
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label class="col-sm-2"></label>
                                 <div class="col-sm-10">
                                    <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                    <button type="submit" class="btn btn-danger m-b-0">Reset</button>
                                 </div>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- UNTUK SETTING COLOR -->
         <div id="styleSelector"></div>
      </div>
   </div>
</div>

@endsection

@section('js')
<script type="text/javascript" src="{{ asset('public/asset_admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/asset_admin/assets/pages/advance-elements/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/asset_admin/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/asset_admin/bower_components/datedropper/datedropper.min.js') }}"></script>
<script>
   $('#dropper-default').dateDropper({
      theme: 'leaf'
   });
</script> 
@endsection