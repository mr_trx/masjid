<?php

namespace App\Http\Controllers;

use Hash;
use Auth;
use File;
use Storage;
use View;

use Carbon\Carbon;
use App\MWaktuIqomah;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;


class ProkesController extends Controller
{
    public function Index()
    {
        $data['m_waktu_iqomah'] = MWaktuIqomah::all();

        return view('prokes', $data);
    }

    public function getWaktuIqomah(){
        $waktuIqomah = MWaktuIqomah::get();
        
        return json_encode(array('data'=>$waktuIqomah));
    }

    public function blankscreen(){
        return view('CountdownBlankScreen');
    }

    public function prokesJumat(){
        return view('prokesSholatJumat');
    }

    public function blankscreenJumat(){
        return view('countdownblankSholatJumat');
    }
    
}
