<?php

namespace App\Http\Controllers;

use Hash;
use Auth;
use Storage;
use View;

use App\User;
use App\MInfoProgramMasjids;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class InfoProgramMasjidController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['m_info_program_masjids'] = MInfoProgramMasjids::all();

        return view('backEND.InfoProgramMasjid.view', $data);
    }

    public function create()
    {
        return view('backEND.InfoProgramMasjid.create');
    }

    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $userID      = Auth::id();
        $today       = date("Y-m-d H:i:s"); 
        $tanggal = "";
        
        if($request->pilih_waktu == "tanggal"){
            $all_data_program = [
                'pilih_waktu'    => $request->pilih_waktu,
                'tanggal'       => date('Y-m-d', strtotime($request->tanggal)),
                'judul'         => $request->judul,
                'konten'        => $request->konten,
                'insert_by'     => $userID,
                'created_at'    =>  $today,
            ];
        } else {
            $all_data_program = [
                'pilih_waktu'    => $request->pilih_waktu,
                'waktu_teks'    => $request->waktu_teks,
                'judul'         => $request->judul,
                'konten'        => $request->konten,
                'insert_by'     => $userID,
                'created_at'    =>  $today,
            ];
        }

        
        // dd($all_data_blogger);die;
    
        $save = MInfoProgramMasjids::insert($all_data_program);
        if($save)
            return redirect('infoprogrammasjid');
        else
            return redirect()->back()->withInput();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data['m_info_program_masjids'] = MInfoProgramMasjids::find($id);
     
        return view('backEND.InfoProgramMasjid.edit')->with(['data' => $data]);
    }

    public function update(Request $request, $id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $userID      = Auth::id();
        $today       = date("Y-m-d H:i:s"); 

        if($request->pilih_waktu == "tanggal"){
            $updateKegiatan = [
                'pilih_waktu'   => $request->pilih_waktu,
                'tanggal'       => date('Y-m-d', strtotime($request->tanggal)),
                'waktu_teks'    => '',
                'judul'         => $request->judul,
                'konten'        => $request->konten,
                'update_by'     => $userID,
                'updated_at'    => $today,
            ];
        } else {
            $updateKegiatan = [
                'pilih_waktu'   => $request->pilih_waktu,
                'tanggal'       => '',
                'waktu_teks'    => $request->waktu_teks,
                'judul'         => $request->judul,
                'konten'        => $request->konten,
                'update_by'     => $userID,
                'updated_at'    => $today,
            ];
        }

       // dd($updateKegiatan);die;
        $updatedata = MInfoProgramMasjids::where('id',$id)->update($updateKegiatan);
        if($updatedata){
            $msg = 'Your data has been successfully updated!';
        return redirect('infoprogrammasjid')->withSuccess($msg);
        } else {
            return redirect()->back()->withInput();
        }
    }

    public function destroy($id)
    {
        $programMasjid = MInfoProgramMasjids::find($id);
        if($programMasjid) {
            $programMasjid->destroy($id);
            $msg = 'Your data has been deleted!';
        } else {
            $msg = 'Please try again!';
        }
        return redirect()
            ->back()
            ->withSuccess($msg); 
    }
}
