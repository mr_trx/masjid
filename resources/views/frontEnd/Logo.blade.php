<div class="text-center">
    <a class="navbar-brand" rel="home" href="#" title="Masjid Al-Ma'mur">
        <img class="img-responsive" style="margin: 0 auto; max-width:160px; margin-top: -28px;margin-left: 20px;"
        src="{{URL::asset('public/image/logo.png')}}">
    </a>
</div>
<div class="text-center" style="font-size:18px;letter-spacing:1px;">
    <script type="text/javascript" src="{{URL::asset('public/js/datahijri.js')}}"></script>
</div>
<div class="text-center">
    <h4 id="jam"></h2>
</div>

<script type="text/javascript">
    window.onload = function() { jam(); }
    function jam() {
        var e = document.getElementById('jam'),
        d = new Date(), h, m, s;
        h = d.getHours();
        m = set(d.getMinutes());
        s = set(d.getSeconds());
        e.innerHTML = h +':'+ m +':'+ s;
        setTimeout('jam()', 1000);
    }
    function set(e) {
        e = e < 10 ? '0'+ e : e;
        return e;
    }
</script>