<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->string('photo');
            $table->string('photo_path');
            $table->integer('role');
            $table->integer('created_by');
            $table->integer('update_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropColumn('tempat_lahir','tanggal_lahir','photo','photo_path','role','created_by','update_by');
    }
}
